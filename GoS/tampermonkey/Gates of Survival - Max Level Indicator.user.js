// ==UserScript==
// @name         Gates of Survival - Max Level Indicator
// @namespace    dex.gos
// @version      1.0
// @description  This script will add the max level for each skill to the profile page, and color code your level in a skill depending how close to max level you are.
// @author       Dex
// @match        https://www.gatesofsurvival.com/game/index.php?page=main
// @grant        none
// @downloadURL  https://dexter15.bitbucket.io/GoS/tampermonkey/Gates of Survival - Max Level Indicator.user.js
// @updateURL    https://dexter15.bitbucket.io/GoS/tampermonkey/Gates of Survival - Max Level Indicator.user.js
// ==/UserScript==

(function() {
    'use strict';
    // Configurable data.
    let logging = true;

    // Static data.
    let levels = {"Agility": 2500, "Alchemy": 2500, "Baking": 2000, "Botany": 2000, "Cooking": 2500, "Crafting": 2500, "Divination": 2000, "Entomology": 2500, "Exploration": 1,    // Exploration was removed from the game and levels were reset. Until it is reimplemented, the max level is effectively 1.
                  "Forging": 2000, "Forestry": 2000, "Firemaking": 2000, "Fishing": 2500, "Fletching": 2000, "Gathering": 2000, "Hunting": 2000, "Jewelcrafting": 2000,
                  "Looting": 2000, "Luck": 3000, "Mining": 2000, "Prayer": 2000, "Runebinding": 2000, "Salvaging": 2500, "Smelting": 2000, "Spellcraft": 2500, "Skinning": 2000,
                  "Summoning": 2500, "Thieving": 2000, "Transmutation": 2500, "Woodworking": 2000, "Attack": 3000, "Arcane Magic": 3000, "Archery": 3000, "Defense": 3000,
                  "Health": 3000, "Slayer": 2500, "Strength": 3000}

    // Methods.
    function logText(text, objectData=null) {
        if (logging) {
            console.log("dex.gos.max_level_indicator    " + text);

            if (objectData !== null) {
                console.log(objectData);
            }
        }
    }

    function processChange() {
        let rgxSkill = /<i>(.*?) Level<\/i>: (.*?) \(Rank #([^)]*)\)/g;

        let jObject = jQuery("div#page > center > table > tbody");
        let skillContent = jObject.html();
        skillContent = skillContent.replace(rgxSkill, (matchedText, skillName, level, rank, offset, originalString) => {
            logText("Found skill: " + skillName);
            let newLevel = "<span style='color:";
            let maxLevel = "?";
            if (levels.hasOwnProperty(skillName)) {
                maxLevel = levels[skillName];
                let levelInt = parseInt(level.replaceAll(/,/g, ""), 10);
                let prcnt = levelInt / maxLevel;
                logText("Parsed level: " + levelInt + "; calculated percent: " + prcnt);

                if (prcnt < 0.34) {
                    newLevel += "DarkSalmon";
                } else if (prcnt < 0.67) {
                    newLevel += "GoldenRod";
                } else if (prcnt < 1) {
                    newLevel += "DarkGoldenRod";
                } else {
                    newLevel += "DarkGreen";
                }
            } else {
                    newLevel += "Tomato";
            }
            newLevel = newLevel + "'>" + level + "/" + maxLevel + "</span>";

            let newText = "<i>" + skillName + " Level</i>: " + newLevel + " (Rank #" + rank + ")";
            logText("skill: " + skillName + "; new text: " + newText);
            return newText;
        });

        // TODO: Add something special for the health.
        rgxSkill = /<i>Health \(([^)]*)\)<\/i>: ([^ ]*) \/ ([^ ]*) HP \(Rank #([^)]*)\)/;

        skillContent = skillContent.replace(rgxSkill, (matchedText, level, health1, health2, rank, offset, originalString) => {
            logText("Found skill Health");
            let newLevel = "<span style='color:";
            let maxLevel = "?";
            maxLevel = levels.Health;
            let levelInt = parseInt(level.replaceAll(/,/g, ""), 10);
            let prcnt = levelInt / maxLevel;
            logText("Parsed level: " + levelInt + "; calculated percent: " + prcnt);

            if (prcnt < 0.34) {
                newLevel += "DarkSalmon";
            } else if (prcnt < 0.67) {
                newLevel += "GoldenRod";
            } else if (prcnt < 1) {
                newLevel += "DarkGoldenRod";
            } else {
                newLevel += "DarkGreen";
            }
            newLevel = newLevel + "'>" + level + "/" + maxLevel + "</span>";

            let newText = "<i>Health (" + newLevel + ")</i>: " + health1 + " / " + health2 + " HP (Rank #" + rank + ")";
            logText("skill: Health; new text: " + newText);
            return newText;
        });

        jObject.html(skillContent);
    }

    function checkAndProcessChange() {
        let hasSkillsSctn = jQuery("div.alert-backpack_box.backpack span:contains('Skills')").length;
        if (hasSkillsSctn > 0) {
            logText("On page with the skills sections.");
            processChange();
        } else {
            //logText("Not on page with the skills sections.");
        }
    }

    logText("Initializng.");
    // Initial/starting/first run code.
    let observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            if (mutation.addedNodes.length > 0) {
                checkAndProcessChange();
            }
        });
    });
    observer.observe(document.getElementById('page'), {attributes: true, childList: true, characterData: true});
})();