// ==UserScript==
// @name         Gates of Survival - Slayer Helper
// @namespace    dex.gos
// @version      1.3.1
// @description  Watches while rolling slayer tasks and moves the button if a desired slayer reward comes up, so you don't accidentally re-roll again and lose it.
// @author       Dex
// @include     https://www.gatesofsurvival.com/game/index.php?page=main
// @grant        none
// @downloadURL  https://dexter15.bitbucket.io/GoS/tampermonkey/Gates of Survival - Slayer Helper.user.js
// @updateURL    https://dexter15.bitbucket.io/GoS/tampermonkey/Gates of Survival - Slayer Helper.user.js
// ==/UserScript==/*


var GoS_SlayerHelper = {
    target: document.getElementById('page'),
    config: {attributes: true, childList: true, characterData: true},
    logging: false,
    desiredRewards: ["Verdite Arrows",
                     "Soul Rupture (Spell)",
                     "Hermious Pouch", "Finsail Pouch", "Coisul Pouch", "Freeflier Pouch", "Giant Leech Pouch", "Adielo Pouch", "Krinaur Pouch", "Metrium Pouch", "Selmilo Pouch", "Jaukeu Pouch", "Henshu Pouch",
                     "Deathlock Pouch", "Gremuire Pouch", "Quinsor Pouch", "Raishan Pouch", "Draken Pouch", "Seama Pouch", "Kintaro Pouch", "Dalmion Pouch", "Freemore Pouch", "Grimjaw Pouch", "Cruelia Pouch",
                     "Quinsail Pouch", "Talu Pouch", "Dark Crescent Pouch", "Dekaria Pouch", "Tinsauro Pouch", "Warphin Pouch", "Darlight Pouch", "Shaulou Pouch", "Riptide Pouch", "Devil Ray Pouch", "Sinspar Pouch",
                     "Steeljaw Pouch", "The Seeker Pouch", "Ripjaw Pouch", "Death Ray Pouch", "Oxford Pouch", "Shawshank Pouch", "Paopu Pouch",
                     "Arrowshafts"],
    logText: function (text) {
        if (GoS_SlayerHelper.logging) {
            console.log("dex.gos.slayerHelper    " + text);
        }
    },
    processChange: function (contentElement) {
        let pageContent = jQuery(".slayer").html();
        let rgxReward = /<b>Rewards<\/b>: You will be rewarded with <b><u>[^<]*<\/u>([^<]*)<\/b> and /i;

        let result = rgxReward.exec(pageContent);
        if (result != null) {
            // Found the slayer reward. Extract it.
            let item = result[1];
            // Remove the "(s)" that always seems to be there (even on items that already have it) and trim the leading and trailing whitespace.
            let i = item.indexOf("(s)");
            if (i !== -1) {
                item = item.substring(0, i);
            }
            item = item.trim();
            this.logText("Slayer reward item: " + item);

            // Check to see if that item is in the list of desired rewards.
            if (this.desiredRewards.includes(item)) {
                // It does. Move the cancel button so the using doesn't accidentally click it. They will have to intentionally click it if they still don't want it.
                let cancelBtn = jQuery("input[value^='Cancel Slayer Task']");
                cancelBtn.css("transform", "translateX(-270px)");
                // Also move the "cancel and downgrade" slayer button, as the user may be trying to use that instead.
                let downgradeBtn = jQuery("input[value^='Cancel & Lower Slayer Task']");
                if (downgradeBtn.length >= 0) {
                    downgradeBtn.css("transform", "translateX(-270px)");
                }

                // For some reason the form (which is the width of the entire row, not just the button) is set up to intercept the click on the button, remove the form handling (including the button press) and submit the data to the server.
                // That means that shifting the button left does nothing, because what the button is meant to do will happen when the user clicks anywhere in that row (like where the button would normally be / used to be).
                // Fix that by removing that extra set up on the form, and adding the submittal it was otherwise doing properly to the button, but still return false so the form submittal doesn't go through too (which would want to reload the whole page
                // instead of allowing the ajah call to do its thing).
                let form1 = cancelBtn.parent();
                form1.off("click");
                cancelBtn.on("click", function(e) {
                    jQuery.post('slayer.php', form1.serialize(), function(data){ jQuery('#page').html(data); });
                    return false;
                });

                if (downgradeBtn.length >= 0) {
                    let form2 = downgradeBtn.parent();
                    form2.off("click");
                    downgradeBtn.on("click", function(e) {
                        jQuery.post('slayer.php', form2.serialize(), function(data){ jQuery('#page').html(data); });
                        return false;
                    });
                }
            }
        }
    },
    checkAndProcessChange: function () {
        let hasSlayerBtn = jQuery("input[value^='Cancel Slayer Task']", this.target).length;
        if (hasSlayerBtn > 0) {
            this.logText("On page with the cancel slayer button.");
            this.processChange(this.target);
        } else {
            //this.logText("Not on page with the cancel slayer button.");
        }
    },
    start: function () {
        let observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                if (mutation.addedNodes.length > 0) {
                    GoS_SlayerHelper.checkAndProcessChange();
                }
            });
        });
        observer.observe(this.target, this.config);
    }
};
GoS_SlayerHelper.start();
if (unsafeWindow.GoS_SlayerHelper === undefined) {
    unsafeWindow.GoS_SlayerHelper = GoS_SlayerHelper;
}

