const MAX_UPGRADE_LVL = 70;
const MAX_ADV_LVL = 30;

var expoRates = [];
for (let lvl = 0; lvl <= MAX_UPGRADE_LVL; lvl++) {
    expoRates[lvl] = {};
    expoRates[lvl]['base'] = 1 - Math.exp(-lvl / 31);
    expoRates[lvl]['gem'] = expoRates[lvl]['base'] * 0.5;
    expoRates[lvl]['relic'] = expoRates[lvl]['base'] * 10;
    expoRates[lvl]['key'] = expoRates[lvl]['base'] * 0.25;
    expoRates[lvl]['orb'] = expoRates[lvl]['base'] * 0.5;
    expoRates[lvl]['scroll'] = expoRates[lvl]['base'] * 0.5;
    expoRates[lvl]['time'] = expoRates[lvl]['base'] * 4;
    if (lvl === 0) {
        expoRates[lvl]['BaseCost'] = 0;
    } else {
        expoRates[lvl]['BaseCost'] = Math.ceil(100000 * Math.pow(1.25, lvl - 1));
    }

    let curCost = expoRates[lvl]['BaseCost'];
    let prevCost = 0;
    if (expoRates[lvl - 1] !== undefined) {
        prevCost = expoRates[lvl - 1]['BaseCost'];
    }
    expoRates[lvl]['TotalCost'] = Math.ceil(curCost + prevCost);
}

var tierCost = [null, 1, 0.752, 0.5, 0.25, 0.1];
var resourceCosts = {"1": [], "2": [], "3": [], "4": [], "5": []};
for (let lvl = 0; lvl <= MAX_UPGRADE_LVL; lvl++) {
    for (let tier = 1; tier < 6; tier++) {
        resourceCosts[tier][lvl] = {};
        if (lvl === 0) {
            resourceCosts[tier][lvl]['BaseCost'] = 0;
            resourceCosts[tier][lvl]['TotalCost'] = 0;
        } else {
            resourceCosts[tier][lvl]['BaseCost'] = Math.ceil(5000 * Math.pow(1.2, lvl - 1) * tierCost[tier]);
            resourceCosts[tier][lvl]['TotalCost'] = Math.ceil(tierCost[tier] * 5000 * (1 - Math.pow(1.2, lvl)) / (-0.2));
        }
    }
}

expoCost = {'gem': {}, 'relic': {}, 'key': {}, 'orb': {}, 'scroll': {}, 'time': {}};
baseExpo = {};
minLevel = 0;
for (let warrior = minLevel; warrior <= MAX_ADV_LVL; warrior++) {
    for (let hunter = minLevel; hunter <= MAX_ADV_LVL; hunter++) {
        for (let mage = minLevel; mage <= MAX_ADV_LVL; mage++) {
            for (let healer = minLevel; healer <= MAX_ADV_LVL; healer++) {
                if (( (warrior + hunter + mage + healer) <= 3) ||
                        (Math.max(warrior, hunter, mage, healer) * 3 < (warrior + hunter + mage + healer) ) ||
                        (warrior == 0 && (Math.min(hunter, mage, healer) == Math.max(hunter, mage, healer) - 1) && ((Math.max(warrior, hunter, mage, healer) - 1) * 3 < (warrior + hunter + mage + healer) )) ||
                        (hunter == 0 && (Math.min(warrior, mage, healer) == Math.max(warrior, mage, healer) - 1) && ((Math.max(warrior, hunter, mage, healer) - 1) * 3 < (warrior + hunter + mage + healer) )) ||
                        (mage == 0 && (Math.min(hunter, warrior, healer) == Math.max(hunter, warrior, healer) - 1) && ((Math.max(warrior, hunter, mage, healer) - 1) * 3 < (warrior + hunter + mage + healer) )) ||
                        (healer == 0 && (Math.min(hunter, mage, warrior) == Math.max(hunter, mage, warrior) - 1) && ((Math.max(warrior, hunter, mage, healer) - 1) * 3 < (warrior + hunter + mage + healer) ))) {
                    let lvl = warrior + hunter + mage + healer;

                    expoCost['gem'][lvl + '_' + (warrior + mage)] = [];
                    expoCost['relic'][lvl + '_' + (warrior + hunter)] = [];
                    expoCost['key'][lvl + '_' + (hunter + healer)] = [];
                    expoCost['orb'][lvl + '_' + (mage + healer)] = [];
                    expoCost['scroll'][lvl + '_' + (hunter + mage)] = [];
                    expoCost['time'][lvl + '_' + (warrior + healer)] = [];

                    expoRates.forEach((expoValue, expoLevel) => {
                        expoCost['gem'][lvl + '_' + (warrior + mage)][expoLevel] = Math.round(lvl / 7 + Math.round((warrior + mage) * expoValue['gem']));
                        expoCost['relic'][lvl + '_' + (warrior + hunter)][expoLevel] = Math.ceil(lvl + Math.round((warrior + hunter) * expoValue['relic'])) + 2;
                        expoCost['key'][lvl + '_' + (hunter + healer)][expoLevel] = Math.floor(lvl / 18 + Math.floor((hunter + healer) * expoValue['key']));
                        expoCost['orb'][lvl + '_' + (mage + healer)][expoLevel] = Math.round(lvl / 20 + Math.round((mage + healer) * expoValue['orb']));
                        expoCost['scroll'][lvl + '_' + (hunter + mage)][expoLevel] = Math.round(lvl / 20 + Math.round((hunter + mage) * expoValue['scroll']));
                        expoCost['time'][lvl + '_' + (warrior + healer)][expoLevel] = Math.round(6 * lvl + 40 - Math.round((warrior + healer) * expoValue['time']));
                    });
                    baseName = warrior + '/' + hunter + '/' + mage + '/' + healer;
                    baseExpo[baseName] = {
                        'level': lvl,
                        'name': baseName,
                        'gem_key': lvl + '_' + (warrior + mage),
                        'relic_key': lvl + '_' + (warrior + hunter),
                        'key_key': lvl + '_' + (hunter + healer),
                        'orb_key': lvl + '_' + (mage + healer),
                        'scroll_key': lvl + '_' + (hunter + mage),
                        'time_key': lvl + '_' + (warrior + healer),
                    };
                }
            }
        }
    }
}

var keyDrops = {
    relicsPerTier: [150, 300, 750, 1500, 5000],
    gemsPerTier: [1, 2, 3, 7, 50],
    mePerTier: [50000, 100000, 250000, 400000, 1000000],
    relicChance: 0.4,
    gemChance: 0.1,
    meChance: 0.25
};


var data = 'var expoCost = ' + JSON.stringify(expoCost);
data += "\n\n";
data += 'var expoRates = ' + JSON.stringify(expoRates);
data += "\n\n";
data += 'var expoBase = ' + JSON.stringify(baseExpo);
data += "\n\n";
data += 'var resourceCosts = ' + JSON.stringify(resourceCosts);
data += "\n\n";
data += 'var keyDrops = ' + JSON.stringify(keyDrops);
// TODO: Add key data here.


function downloadJs(data, fileName) {
    this.fileName = fileName;

    this.getDownloadLink = function () {
        var type = 'data:application/octet-stream;charset=utf-8';

        if (typeof btoa === 'function') {
            type += ';base64';
            data = btoa(data);
        } else {
            data = encodeURIComponent(data);
        }

        return this.downloadLink = this.downloadLink || type + ',' + data;
    };

    this.getLinkElement = function (linkText) {
        var downloadLink = this.getDownloadLink();
        var fileName = this.fileName;
        this.linkElement = this.linkElement || (function() {
            var a = document.createElement('a');
            a.innerHTML = linkText || '';
            a.href = downloadLink;
            a.download = fileName;
            return a;
        }());
        return this.linkElement;
    };

    // call with removeAfterDownload = true if you want the link to be removed after downloading
    this.download = function (removeAfterDownload) {
        var linkElement = this.getLinkElement();
        linkElement.style.display = 'none';
        document.body.appendChild(linkElement);
        linkElement.click();
        if (removeAfterDownload) {
            document.body.removeChild(linkElement);
        }
    };
}

var temp = new downloadJs(data, "MQO_Expo_Base.js");
temp.download(true);
