// ==UserScript==
// @name         MidenQuest - Perk Button Hider
// @namespace    dex.mqo
// @version      1.0
// @description  Hides the buttons for activating or upgrading perks you aren't interested in. Also hides the buttons to spend resources on extending the perk timer for resources you don't want to spend.
// @author       Dex
// @include      http://www.midenquest.com/Game.aspx
// @include      http://midenquest.com/Game.aspx
// @downloadURL  https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest - Perk Button Hider.user.js
// @updateURL    https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest - Perk Button Hider.user.js
// ==/UserScript==


var MQO_PerkButtonHider = {
    target: document.getElementById('ContentLoad'),
    config: {attributes: true, childList: true, characterData: true},
    logging: false,
    preferredPerks: ["Drunk", "Enraged", "Lucky", "Pitied", "Hoarder", "Captain"],
    preferredResources: ["Ore", "Plant", "Wood", "Fish"],
    perkList: ["Drunk", "Enraged", "Lucky", "Pitied", "Hoarder", "Captain"],
    resourceList: ["Ore", "Plant", "Wood", "Fish"],
    logText: function (text) {
        if (MQO_PerkButtonHider.logging) {
            console.log("dex.mqo.perkButtons    " + text);
        }
    },
    processChange: function (contentElement) {
        // Save this for later. This is the overall window that the configure button will be added to.
        let perkWindow = jQuery('button#btnUpgradePerk1').parent().parent().parent().parent();

        // Loop and remove the buttons the user doesn't want.
        for (let i = 0; i < this.perkList.length; i++) {
            if (!this.preferredPerks.includes(this.perkList[i])) {
                // This is not one the perks the user cares about. Remove its buttons.
                jQuery('button#btnUpgradePerk' + (i+1), contentElement).remove();
                jQuery('button#btnActivatePerk' + (i+1), contentElement).remove();
                jQuery('button#btnActivatePerk2' + (i+1), contentElement).remove();
            }
        }

        for (let i = 0; i < this.resourceList.length; i++) {
            let resource = this.resourceList[i];
            if (!this.preferredResources.includes(resource)) {
                // This is not one the resources the user cares about. Remove its buttons.
                jQuery('button#btnActivate' + resource, contentElement).remove();
            }
        }

        perkWindow.append('<div id="dex_perk_settings_btn_div" style="text-align: center" class=""><button id="dex_perk_settings_btn" class="darkBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 9pt;" role="button" aria-disabled="false"><span class="ui-button-text">Configure Button Hiding</span></button></div>');
        MQO_PerkButtonHider.settingsButton = jQuery('#dex_perk_settings_btn');
        MQO_PerkButtonHider.settingsButton.click(function (event) {
            let settingWindowHtml = '<div id="dex_perk_settings" class="ui-widget-content ui-corner-all" style="position: absolute; top: 25%; left: 10%; width: 400px; z-index: 200; border: solid thin;"><br />';
            settingWindowHtml += '<div id="dex_perk_settings_perks" style="display: inline-block; width: 49%; border-right: solid thin">For which perks do you want the activation buttons displayed?<br />';
            for (let perk of MQO_PerkButtonHider.perkList) {
                settingWindowHtml += '<input type="checkbox" name="' + perk + (MQO_PerkButtonHider.preferredPerks.includes(perk) ? '" checked="checked"' : '"') + ' />' + perk + '<br />';
            }
            settingWindowHtml += '</div><div id="dex_perk_settings_resources" style="display: inline-block; vertical-align:top; width: 50%">For which resources do you want the activation buttons displayed?<br />';
            for (let resource of MQO_PerkButtonHider.resourceList) {
                settingWindowHtml += '<input type="checkbox" name="' + resource + (MQO_PerkButtonHider.preferredResources.includes(resource) ? '" checked="checked"' : '"') + ' />' + resource + '<br />';
            }
            settingWindowHtml += '</div><br /><br /><div id="dex_perk_settings_save_btn" style="text-align: center" class=""><button class="darkBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 9pt;" role="button" aria-disabled="false" onclick="MQO_PerkButtonHider.saveConfig();"><span class="ui-button-text">Save Configuration</span></button></div><br />';

            jQuery("#ContentLoad").append(settingWindowHtml);
        });
    },
    checkAndProcessChange: function () {
        var hasPerk = jQuery('button#btnUpgradePerk1', this.target).length;
        if (hasPerk > 0) {
            MQO_PerkButtonHider.logText("On Perk Page");
            MQO_PerkButtonHider.processChange(this.target);
        } else {
            // MQO_PerkButtonHider.logText("Not On Perk Page");
        }
    },
    saveConfig: function() {
        let preferences = {};

        let newPerks = [];
        jQuery("#dex_perk_settings_perks").find("input:checkbox").each(function() {
            if (this.checked) {
                newPerks.push($(this).attr("name"));
            }
        });
        MQO_PerkButtonHider.preferredPerks = newPerks;
        preferences.preferredPerks = newPerks;

        let newResources = [];
        jQuery("#dex_perk_settings_resources").find("input:checkbox").each(function() {
            if (this.checked) {
                newResources.push($(this).attr("name"));
            }
        });
        MQO_PerkButtonHider.preferredResources = newResources;
        preferences.preferredResources = newResources;

        let preferencesString = JSON.stringify(preferences);
        localStorage.dex_mqo_perkButtonPrefs = preferencesString;

        jQuery("#dex_perk_settings").remove();
        // These two are base game calls to reload the Perks page.
        sendRequestContentFill('getInfoPerk.aspx?null=');
        fightengaged = 0;
    },
    start: function () {
        let preferencesString = localStorage.getItem("dex_mqo_perkButtonPrefs");
        MQO_PerkButtonHider.logText("Stringified preferencesString from storage: " + preferencesString);

        if (preferencesString != null) {
            let preferences = JSON.parse(preferencesString);
            MQO_PerkButtonHider.preferredPerks = preferences.preferredPerks;
            MQO_PerkButtonHider.preferredResources = preferences.preferredResources;
        }

        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                if (mutation.addedNodes.length > 0) {
                    MQO_PerkButtonHider.checkAndProcessChange();
                }
            });
        });
        observer.observe(this.target, this.config);
    }
};
MQO_PerkButtonHider.start();
if (unsafeWindow.MQO_PerkButtonHider === undefined) {
    unsafeWindow.MQO_PerkButtonHider = MQO_PerkButtonHider;
}

