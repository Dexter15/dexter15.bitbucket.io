// ==UserScript==
// @name         MidenQuest - WebsocketWrapper
// @namespace    https://github.com/Altizar/Altizar.github.io
// @version      0.8
// @description  Wraps the MQO websocket so other scripts can hook into the data coming back and respond/modify/add accordingly.
// @author       Altizar
// @include      http://www.midenquest.com/Game.aspx
// @include      http://midenquest.com/Game.aspx
// @downloadURL  https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest.WebsocketWrapper.user.js
// @updateURL    https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest.WebsocketWrapper.user.js
// ==/UserScript==

var MQO_WebsocketWrapper = {
    websocket : null,
    ws : null,
    callbacks : [],
    start : function(websocket) {
        console.log(websocket);
        this.websocket = websocket;
        this.ws = websocket;
        this.callbacks = [];
        this.addCallback(ws.onmessage);
        this.ws.onmessage = this.onMessage;
    },
    addCallback : function(callback, context=null) {
        if (context === null) {
            context = this;
        }
        return this.callbacks.push({cb: callback, ctx: context});
    },
    removeCallback : function(callback) {
        const index = this.callbacks.findIndex((el) => el.cb === callback);
        if (index > -1) {
            this.callbacks.splice(index, 1);
            return true;
        }
        return false;
    },
    onMessage : function(event) {
        var callback, i, len, ref, results;
        ref = MQO_WebsocketWrapper.callbacks;
        results = [];
        for (i = 0, len = ref.length; i < len; i++) {
            callback = ref[i];
            results.push(callback.cb.call(callback.ctx, event));
        }
        return results;
    },
    WebSocketLogger : function(data) {
        console.log(data);
    }
};

MQO_WebsocketWrapper.start(ws);
//MQO_WebsocketWrapper.addCallback(MQO_WebsocketWrapper.WebSocketLogger);
if (unsafeWindow.MQO_WebsocketWrapper === undefined) {
    unsafeWindow.MQO_WebsocketWrapper = MQO_WebsocketWrapper;
}

