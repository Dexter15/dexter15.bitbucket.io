// ==UserScript==
// @name         MidenQuest - Transform Helper
// @namespace    dex.mqo
// @version      1.0
// @description  Watches while transforming gear and moves the button if a miden III enchant comes up, so you don't accidentally re-roll again and lose it.
// @author       Dex
// @include      http://www.midenquest.com/Game.aspx
// @include      http://midenquest.com/Game.aspx
// @downloadURL  https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest - Transform Helper.user.js
// @updateURL    https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest - Transform Helper.user.js
// ==/UserScript==/*


var MQO_TransformHelper = {
    target: document.getElementById('ContentLoad'),
    config: {attributes: true, childList: true, characterData: true},
    logging: false,
    logText: function (text) {
        if (MQO_TransformHelper.logging) {
            console.log("dex.mqo.transformHelper    " + text);
        }
    },
    processChange: function (contentElement) {
        // Unfortunately, there is nothing distinguishable among all of the sub-elments leading into the enchantment data, so hardcode the exact DOM path to it.
        let enchantmentsNode = jQuery(contentElement).children().eq(6).children().eq(0).children().eq(0).children().eq(2);
        //this.logText("Enchants node HTML: " + enchantmentsNode.html());

        let midenRow = enchantmentsNode.children(":contains('All Stats')");
        if (midenRow.length > 0) {
            // There is a miden enchant on this piece of gear. Check to see if it is a level 3 or more (or enough level 2s and 1s to _look_ like a level 3).
            let enchantTtlTxt = midenRow.children().eq(1).text();
            this.logText("enchantTtl text: " + enchantTtlTxt);

            // Parse the total to an integer and see if it is equal to or greater than 4 (the total one level 3 enchant would give).
            let midenTtl = parseInt(enchantTtlTxt, 10);
            if (midenTtl >= 4) {
                // There is (probably) a miden level 3 enchant on this gear. Move the transform button so the user doesn't accidentally click it (they will have to intentionally move to it and click it).
                this.logText("A miden level 3 enchant has (probably) been found. Moving button.");
                let transformBtn = jQuery("button#btnTransform", contentElement);
                transformBtn.css("transform", "translateX(-150px)");
            }
        }
    },
    checkAndProcessChange: function () {
        let hasTranformBtn = jQuery('button#btnTransform', this.target).length;
        if (hasTranformBtn > 0) {
            this.logText("On Page with Transform");
            this.processChange(this.target);
        } else {
            this.logText("Not On Page with Transform");
        }
    },
    start: function () {
        let observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                if (mutation.addedNodes.length > 0) {
                    MQO_TransformHelper.checkAndProcessChange();
                }
            });
        });
        observer.observe(this.target, this.config);
    }
};
MQO_TransformHelper.start();
if (unsafeWindow.MQO_TransformHelper === undefined) {
    unsafeWindow.MQO_TransformHelper = MQO_TransformHelper;
}

