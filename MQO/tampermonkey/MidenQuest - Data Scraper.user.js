// ==UserScript==
// @name         MidenQuest - Data Scraper
// @namespace    dex.mqo
// @version      1.1
// @description  Provides a walkthrough to visit most pages of the game and gather information from them for the purpose of exporting to an investment calculator (like Kontors' spreadsheet).
// @author       Dex
// @include      http://www.midenquest.com/Game.aspx
// @include      http://midenquest.com/Game.aspx
// @downloadURL  https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest - Data Scraper.user.js
// @updateURL    https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest - Data Scraper.user.js
// ==/UserScript==


var MQO_DataScraper = {
    target: document.getElementById('ContentLoad'),
    config: {attributes: true, childList: true, characterData: true},
    logging: false,
    data: {
        player: {
            primaryTS: "", focus: "",
            relic: {
                bonusExp: "", bonusRes: "", bonusWork: "", bonusItem: "", bonusLuck: "",
                costExp: "", costRes: "", costWork: "", costItem: "", costLuck: ""
            },
            level: {
                gathering: "", mining: "", scouting: "", selling: "", woodcutting: "", fishing: "", global: "",
            },
            gem: {
                bonusItem: "", bonusGold: "", bonusResources: "", bonusExperience: "", perkReduct: "", runeSlots: "", expoSlots: ""
            },
            perk: {
                drunkPrct: "", enragedPrct: "", luckyPrct: "", pitiedActns: "", hoarderPrct: "", captainPrct: ""
            },
            island: {
                gamblerLvl: "", overseerLvl: "", hutExpPrct: ""
            }
        },
        kingdom: {
            taxPrct: "", bonusExp: "", bonusTsRsrcPrct: "", bonusTsLuckPrct: "", inns: ""
        }
    },
    logText: function (text) {
        if (this.logging) {
            console.log("dex.mqo.dataScraper    " + text);
        }
    },
    copyStringToClipboard: function(str) {
        // Create new element
        var el = document.createElement('textarea');
        // Set value (string to be copied)
        el.value = str;
        // Set non-editable to avoid focus and move outside of view
        el.setAttribute('readonly', '');
        el.style = {position: 'absolute', left: '-9999px'};
        document.body.appendChild(el);
        // Select text inside element
        el.select();
        // Copy text to clipboard
        document.execCommand('copy');
        // Remove temporary element
        document.body.removeChild(el);
    },
    addWizardButton: function () {
        jQuery("button#btnLogout").after('<button id="dex_btnScraperWizard" class="darkBtn doubleLined ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">Data Scraper Wizard</span></button>');
        jQuery("button#dex_btnScraperWizard").click(function() {
            let wizardEl = jQuery("div#dex_data_scraper_wizard");
            if (wizardEl.length > 0) {
                wizardEl.show();
            } else {
                // Add the styles and wizard overlay.
                jQuery("head").append("<style>\n    div#dex_data_scraper_wizard ol {\n        list-style: decimal inside;\n    }\n    div#dex_data_scraper_wizard li {\n       margin-bottom: 10px;\n    }\n    div#dex_data_scraper_wizard a {\n        color: blue;\n    }\n</style>");
                jQuery("body").append('<div id="dex_data_scraper_wizard" class="ui-widget-content ui-corner-all" style="position: absolute; top: 25%; left: 7%; width: 200px; z-index: 210; border: solid thin; background-image: none; background-color: #DDD; padding: 5px; text-align: left">\n    <ol>\n        <li>Open your primary TS Relics page.<br>\n        <a onclick="sendRequestContentFill(\'getTradeskill.aspx?w=1&d=1&null=\'); fightengaged = 0; return false;" href="#nothing">Scouting</a> <a onclick="sendRequestContentFill(\'getTradeskill.aspx?w=4&d=1&null=\'); fightengaged = 0; return false;" href="#nothing">Gathering</a> <a onclick="sendRequestContentFill(\'getTradeskill.aspx?w=3&d=1&null=\'); fightengaged = 0; return false;" href="#nothing">Mining</a> <a onclick="sendRequestContentFill(\'getTradeskill.aspx?w=5&d=1&null=\'); fightengaged = 0; return false;" href="#nothing">Logging</a> <a onclick="sendRequestContentFill(\'getTradeskill.aspx?w=6&d=1&null=\'); fightengaged = 0; return false;" href="#nothing">Fishing</a> <a onclick="sendRequestContentFill(\'getTradeskill.aspx?w=2&d=1&null=\'); fightengaged = 0; return false;" href="#nothing">Selling</a></li>\n        <li>Now click on your primary skill\'s focus tab.</li>\n        <li>Open your <a onclick="sendRequestContentFill(\'getInfoPlayer.aspx?null=\'); fightengaged = 0; return false;" href="#nothing">account</a> page.</li>\n        <li>Open your account <a onclick="sendRequestContentFill(\'getInfoBonus.aspx?null=\'); fightengaged = 0; return false;" href="#nothing">gem boosts</a> page.</li>\n        <li>Open your <a onclick="sendRequestContentFill(\'getInfoPerk.aspx?null=\'); fightengaged = 0; return false;" href="#nothing">perks</a> page.</li>\n        <li>Open your <a onclick="sendRequestContentFill(\'getNavigation.aspx?screen=2&null=\'); fightengaged = 0; return false;" href="#nothing">island</a> page.</li>\n        <li>Open your <a onclick="sendRequestContentFill(\'getKingdom.aspx?null=\'); fightengaged = 0; return false;" href="#nothing">kingdom</a> page.</li>\n        <li>Click <a onclick="MQO_DataScraper.copyStringToClipboard(JSON.stringify(MQO_DataScraper.data)); return false;" href="#nothing">here</a> to export the results in JSON to your clipboard.</li>\n        <li>Paste them into an import page on a calculator of your choice.</li>\n        <li><a href="#nothing" return="" onclick="jQuery(\'div#dex_data_scraper_wizard\').hide(); return false;">Close</a> this window.</li>\n    </ol>\n</div>');
            }
        });
    },
    scrapeDataRelics: function (contentElement) {
        this.data.player.primaryTS = jQuery('span#WorkLvl').parent().prev().html();
        this.logText("Added Primary Tradeskill: " + this.data.player.primaryTS);

        let bonusExp = jQuery('div#BonusExp', contentElement).html();
        this.data.player.relic.bonusExp = bonusExp.replace(/[+, ]/g, "");
        this.logText("Added Relic Experience Bonus: " + this.data.player.relic.bonusExp);
        let bonusRes = jQuery('div#BonusRes', contentElement).html();
        this.data.player.relic.bonusRes = bonusRes.replace(/[+, ]/g, "");
        this.logText("Added Relic Resource Bonus: " + this.data.player.relic.bonusRes);
        let bonusWork = jQuery('div#BonusWork', contentElement).html();
        this.data.player.relic.bonusWork = bonusWork.replace(/[+, ]/g, "");
        this.logText("Added Relic Workload Bonus: " + this.data.player.relic.bonusWork);
        let bonusItem = jQuery('div#BonusChest', contentElement).html();
        this.data.player.relic.bonusItem = bonusItem.replace(/[+, %]/g, "");
        this.logText("Added Relic Item Log Bonus: " + this.data.player.relic.bonusItem);
        let bonusLuck = jQuery('div#BonusTier', contentElement).html();
        this.data.player.relic.bonusLuck = bonusLuck.replace(/[+, %]/g, "");
        this.logText("Added Relic Resource Luck Bonus: " + this.data.player.relic.bonusLuck);

        this.data.player.relic.costExp = jQuery('div#ExpRel', contentElement).html().replace(" relics", "");
        this.logText("Added Relic Experience Bonus Cost: " + this.data.player.relic.costExp);
        this.data.player.relic.costRes = jQuery('div#ResRel', contentElement).html().replace(" relics", "");
        this.logText("Added Relic Resource Bonus Cost: " + this.data.player.relic.costRes);
        this.data.player.relic.costWork = jQuery('div#WorkRel', contentElement).html().replace(" relics", "");
        this.logText("Added Relic Workload Bonus Cost: " + this.data.player.relic.costWork);
        this.data.player.relic.costItem = jQuery('div#ChestRel', contentElement).html().replace(" relics", "");
        this.logText("Added Relic Item Log Bonus Cost: " + this.data.player.relic.costItem);
        this.data.player.relic.costLuck = jQuery('div#TierRel', contentElement).html().replace(" relics", "");
        this.logText("Added Relic Resource Luck Bonus Cost: " + this.data.player.relic.costLuck);
    },
    scrapeDataFocus: function (contentElement) {
        let btnId = jQuery("span.ui-button-text:contains('X')", contentElement).parent().attr('id');
        this.logText("Focused button: " + btnId);
        let tier = btnId.substring(8);
        if (tier == "0") {
            this.data.player.focus = "None";
        } else {
            this.data.player.focus = "T" + tier;
        }
        this.logText("Added Focus: " + this.data.player.focus);
    },
    scrapeDataAccount: function (contentElement) {
        let searchHtml = jQuery(contentElement).html();

        let rgxGathering = /<div>Gathering<\/div><div>Lvl. ([^<]*)<\/div>/i;
        let rgxMining = /<div>Mining<\/div><div>Lvl. ([^<]*)<\/div>/i;
        let rgxScouting = /<div>Scouting<\/div><div>Lvl. ([^<]*)<\/div>/i;
        let rgxSelling = /<div>Selling<\/div><div>Lvl. ([^<]*)<\/div>/i;
        let rgxWoodcutting = /<div>Woodcutting<\/div><div>Lvl. ([^<]*)<\/div>/i;
        let rgxFishing = /<div>Fishing<\/div><div>Lvl. ([^<]*)<\/div>/i;
        let rgxGlobalLvl = /<div>([^<]*)<\/div>\s*\n\s*<div style="white-space:normal;">Global Level<\/div>/i;

        let result = rgxGathering.exec(searchHtml);
        if (result != null) {
            // Found the gathering level data. Pull the data out.
            this.data.player.level.gathering = result[1].replace(/[, ]/g, "");
            this.logText("Added Gathering Level: " + this.data.player.level.gathering);
        }
        result = rgxMining.exec(searchHtml);
        if (result != null) {
            // Found the mining level data. Pull the data out.
            this.data.player.level.mining = result[1].replace(/[, ]/g, "");
            this.logText("Added Mining Level: " + this.data.player.level.mining);
        }
        result = rgxScouting.exec(searchHtml);
        if (result != null) {
            // Found the scouting level data. Pull the data out.
            this.data.player.level.scouting = result[1].replace(/[, ]/g, "");
            this.logText("Added Scouting Level: " + this.data.player.level.scouting);
        }
        result = rgxSelling.exec(searchHtml);
        if (result != null) {
            // Found the selling level data. Pull the data out.
            this.data.player.level.selling = result[1].replace(/[, ]/g, "");
            this.logText("Added Selling Level: " + this.data.player.level.selling);
        }
        result = rgxWoodcutting.exec(searchHtml);
        if (result != null) {
            // Found the woodcutting level data. Pull the data out.
            this.data.player.level.woodcutting = result[1].replace(/[, ]/g, "");
            this.logText("Added Woodcutting Level: " + this.data.player.level.woodcutting);
        }
        result = rgxFishing.exec(searchHtml);
        if (result != null) {
            // Found the fishing level data. Pull the data out.
            this.data.player.level.fishing = result[1].replace(/[, ]/g, "");
            this.logText("Added Fishing Level: " + this.data.player.level.fishing);
        }
        result = rgxGlobalLvl.exec(searchHtml);
        if (result != null) {
            // Found the global level data. Pull the data out.
            this.data.player.level.global = result[1].replace(/[, ]/g, "");
            this.logText("Added Global Level: " + this.data.player.level.global);
        }

        this.addWizardButton();
    },
    scrapeDataGems: function (contentElement) {
        jQuery("div.InfoBonusBlock", contentElement).each(function(index) {
            let bonusLabel = jQuery(this).children("div.InfoBonusTitle").clone().children().remove().end().text();
            //MQO_DataScraper.logText("bonusLabel: " + bonusLabel);

            if (bonusLabel == "Item Drop Rate") {
                let bonusValue = jQuery(this).children("div.InfoBonusValue").clone().children().remove().end().text();
                MQO_DataScraper.data.player.gem.bonusItem = bonusValue.replace(/[+ %]/g, "");
                MQO_DataScraper.logText("Added Gem Item Log Bonus: " + MQO_DataScraper.data.player.gem.bonusItem);
            } else if (bonusLabel == "Gold Drop Rate") {
                let bonusValue = jQuery(this).children("div.InfoBonusValue").clone().children().remove().end().text();
                MQO_DataScraper.data.player.gem.bonusGold = bonusValue.replace(/[+ %]/g, "");
                MQO_DataScraper.logText("Added Gem Gold Drop Bonus: " + MQO_DataScraper.data.player.gem.bonusGold);
            } else if (bonusLabel == "Resources Found") {
                let bonusValue = jQuery(this).children("div.InfoBonusValue").clone().children().remove().end().text();
                MQO_DataScraper.data.player.gem.bonusResources = bonusValue.replace(/[+ %]/g, "");
                MQO_DataScraper.logText("Added Gem Resources Bonus: " + MQO_DataScraper.data.player.gem.bonusResources);
            } else if (bonusLabel == "Global Exp Rate") {
                let bonusValue = jQuery(this).children("div.InfoBonusValue").clone().children().remove().end().text();
                MQO_DataScraper.data.player.gem.bonusExperience = bonusValue.replace(/[+ %]/g, "");
                MQO_DataScraper.logText("Added Gem Experience Bonus: " + MQO_DataScraper.data.player.gem.bonusExperience);
            } else if (bonusLabel == "Perk Act. Reduction") {
                let bonusValue = jQuery(this).children("div.InfoBonusValue").clone().children().remove().end().text();
                MQO_DataScraper.data.player.gem.perkReduct = bonusValue.replace(/[+ %]/g, "");
                MQO_DataScraper.logText("Added Gem Perk Activation Reduction: " + MQO_DataScraper.data.player.gem.perkReduct);
            } else if (bonusLabel == "Rune Slots") {
                let bonusValue = jQuery(this).children("div.InfoBonusValue").clone().children().remove().end().text();
                MQO_DataScraper.data.player.gem.runeSlots = bonusValue.replace(" Slots", "");
                MQO_DataScraper.logText("Added Gem Rune Slots: " + MQO_DataScraper.data.player.gem.runeSlots);
            } else if (bonusLabel == "Expedition Slots") {
                let bonusValue = jQuery(this).children("div.InfoBonusValue").clone().children().remove().end().text();
                MQO_DataScraper.data.player.gem.expoSlots = bonusValue.replace(" Slots", "");
                MQO_DataScraper.logText("Added Gem Expo Slots: " + MQO_DataScraper.data.player.gem.expoSlots);
            }
        });
    },
    scrapeDataPerks: function (contentElement) {
        let rgxPrct = /(\d+\.?\d*)% chance/i;
        let rgxLuckyPrct = /(\d+\.?\d*)% bonus/i;
        let rgxPitiedActns = /none was found for (\d+) actions/i;

        jQuery("div.ui-widget-content > div", contentElement).each(function(index) {
            let children = jQuery(this).children();
            let perkLabel = children.first().children().first().html();
            MQO_DataScraper.logText("perkLabel: " + perkLabel);

            if (perkLabel === null) {
                return true;
            }

            if (perkLabel.startsWith("Drunk lvl.") || perkLabel.startsWith("Enraged lvl.") || perkLabel.startsWith("Hoarder lvl.") || perkLabel.startsWith("Captain lvl.")) {
                MQO_DataScraper.logText("Found the 'drunk', 'enraged', 'hoarder' or 'captain' perk.");
                // Get the second child, as that contains the descriptive text with the percentage in it.
                let desc = jQuery(children.get(1)).text();
                let result = rgxPrct.exec(desc);
                if (result != null) {
                    // Found the perk percentage data. Pull the data out.
                    if (perkLabel.startsWith("Drunk lvl.")) {
                        MQO_DataScraper.data.player.perk.drunkPrct = result[1];
                        MQO_DataScraper.logText("Added Drunk Percent: " + MQO_DataScraper.data.player.perk.drunkPrct);
                    } else if (perkLabel.startsWith("Enraged lvl.")) {
                        MQO_DataScraper.data.player.perk.enragedPrct = result[1];
                        MQO_DataScraper.logText("Added Enraged Percent: " + MQO_DataScraper.data.player.perk.enragedPrct);
                    } else if (perkLabel.startsWith("Hoarder lvl.")) {
                        MQO_DataScraper.data.player.perk.hoarderPrct = result[1];
                        MQO_DataScraper.logText("Added Hoarder Percent: " + MQO_DataScraper.data.player.perk.hoarderPrct);
                    } else if (perkLabel.startsWith("Captain lvl.")) {
                        MQO_DataScraper.data.player.perk.captainPrct = result[1];
                        MQO_DataScraper.logText("Added Captain Percent: " + MQO_DataScraper.data.player.perk.captainPrct);
                    }
                }
            } else if (perkLabel.startsWith("Lucky lvl.")) {
                MQO_DataScraper.logText("Found the 'lucky' perk.");
                // Get the second child, as that contains the descriptive text with the percentage in it.
                let desc = jQuery(children.get(1)).text();
                let result = rgxLuckyPrct.exec(desc);
                if (result != null) {
                    // Found the lucky percentage data. Pull the data out.
                    MQO_DataScraper.data.player.perk.luckyPrct = result[1];
                    MQO_DataScraper.logText("Added Lucky Bonus: " + MQO_DataScraper.data.player.perk.luckyPrct);
                }
            } else if (perkLabel.startsWith("Pitied lvl.")) {
                MQO_DataScraper.logText("Found the 'pitied' perk.");
                // Get the second child, as that contains the descriptive text with the percentage in it.
                let desc = jQuery(children.get(1)).text();
                let result = rgxPitiedActns.exec(desc);
                if (result != null) {
                    // Found the pitied action data. Pull the data out.
                    MQO_DataScraper.data.player.perk.pitiedActns = result[1];
                    MQO_DataScraper.logText("Added Pitied Actions: " + MQO_DataScraper.data.player.perk.pitiedActns);
                }
            }
        });
    },
    scrapeDataIsland: function (contentElement) {
        let node = jQuery("tspan:contains('Gambler Post')");
        if (node.length > 0) {
            let level = node.parent().next().text().replace("L. ", "");
            this.data.player.island.gamblerLvl = level;
            this.logText("Added Gambler level: " + this.data.player.island.gamblerLvl);
        } else {
            this.logText("No Gambler's Post on the island.");
        }

        node = jQuery("tspan:contains('Overseer's circle')");
        if (node.length > 0) {
            let level = node.parent().next().text().replace("L. ", "");
            this.data.player.island.overseerLvl = level;
            this.logText("Added Overseer level: " + this.data.player.island.overseerLvl);
        } else {
            this.logText("No Overseer's Circle on the island.");
        }

        // Figure out what the skill building is named.
        let searchStr = "div.TitleBlock:contains('" + this.data.player.primaryTS + "')";
        let tsHut = jQuery(searchStr);
        if (tsHut.length > 0) {
            // This seems to be returning the wrong node (and therefore the wrong text), but the regex allows it to work out in the end, so I guess it stays.
            let tsHutTxt = tsHut.next().text();
            this.logText("tsHutTxt: " + tsHutTxt);
            let rgxPrct = /\+(\d+\.?\d*) % exp,/i;
            let result = rgxPrct.exec(tsHutTxt);
            if (result != null) {
                // Found the percentage data. Pull the data out.
                this.data.player.island.hutExpPrct = result[1];
                this.logText("Added TS hut percentage: " + this.data.player.island.hutExpPrct);
            }
        } else {
            this.logText("No TS hut on the island.");
        }
    },
    scrapeDataKingdom: function (contentElement) {
        let node = jQuery("div#SubScreenBonus > div > div:contains('Taxes:')");
        if (node.length > 0) {
            // Found the Taxes label, move to the value.
            let taxTxt = node.next().text();
            this.logText("taxTxt: " + taxTxt);
            let rgxPrct = /(\d+\.?\d*) ?%/i;
            let result = rgxPrct.exec(taxTxt);
            if (result != null) {
                // Found the percentage data. Pull the data out.
                this.data.kingdom.taxPrct = result[1];
                this.logText("Added kingdom tax percentage: " + this.data.kingdom.taxPrct);
            }
        }

        node = jQuery("div#SubScreenTS > div > div:contains(' Exp')");
        if (node.length > 0) {
            // Found the general experience bonus text (not the combat experience bonus).
            let expTxt = node.text();
            this.logText("expTxt: " + expTxt);
            let rgxPrct = /\+(\d+\.?\d*) Exp/i;
            let result = rgxPrct.exec(expTxt);
            if (result != null) {
                // Found the bonus data. Pull the data out.
                this.data.kingdom.bonusExp = result[1];
                this.logText("Added kingdom experience bonus: " + this.data.kingdom.bonusExp);
            }
        }

        // Get the node containing the collection of nodes that have the TS boost percentages.
        node = jQuery("div#SubScreenTS").children().eq(1);
        // These nodes don't have any identifier to tell which boost is which, so we just have to hardcode the order.
        let bonusMap = {"Mining": "", "Gathering": "", "Woodcutting": "", "Fishing": "", "Scouting": "", "Selling": ""};
        let mapKeys = Object.keys(bonusMap);

        node.children().each(function(index) {
            if (index == 0) {
                // Skip the "Res %" label.
                return true;
            }

            let bonusTsRsrcPrct = jQuery(this).text();
            if (jQuery(this).css('background-color') == 'rgb(211, 247, 160)') {
                // This resource bonus is currently being boosted. Subtract 10 to account for that.
                let tempInt = parseInt(bonusTsRsrcPrct, 10) - 10;
                bonusTsRsrcPrct = tempInt.toString();
            }

            var key = mapKeys[index - 1];
            bonusMap[key] = bonusTsRsrcPrct;
        });

        // Now that the map is filled out, use it to get the percentage for the primary TS skill.
        this.data.kingdom.bonusTsRsrcPrct = bonusMap[this.data.player.primaryTS];
        this.logText("Added kingdom tradeskill resource bonus percentage: " + this.data.kingdom.bonusTsRsrcPrct);

        // Get the node containing the collection of nodes that have the TS boost percentages.
        node = jQuery("div#SubScreenTS").children().eq(2);
        // These nodes don't have any identifier to tell which boost is which, so we just have to hardcode the order.
        // Reset/clear the original map.
        bonusMap = {"Mining": "", "Gathering": "", "Woodcutting": "", "Fishing": "", "Scouting": "", "Selling": ""};

        node.children().each(function(index) {
            if (index == 0) {
                // Skip the "Luck %" label.
                return true;
            }

            let bonusTsLuckPrct = jQuery(this).text();
            if (jQuery(this).css('background-color') == 'rgb(211, 247, 160)') {
                // This resource bonus is currently being boosted. Subtract ? to account for that.
            }

            var key = mapKeys[index - 1];
            bonusMap[key] = bonusTsLuckPrct;
        });

        // Now that the map is filled out, use it to get the percentage for the primary TS skill.
        this.data.kingdom.bonusTsLuckPrct = bonusMap[this.data.player.primaryTS];
        this.logText("Added kingdom tradeskill luck bonus percentage: " + this.data.kingdom.bonusTsLuckPrct);

        // Get the section containing the Inn data.
        node = jQuery("div#SubScreenBuild").children("div:contains('Inn')");
        node = node.children().last();
        let innCntTxt = node.text();
        this.logText("Text with the count of the inns: " + innCntTxt);
        this.data.kingdom.inns = innCntTxt.substring(1).trim();
        this.logText("Added the number of Inns from the Kingdom: " + this.data.kingdom.inns);
    },
    checkAndScrape: function () {
        let isRelicPage = jQuery('div#BonusExp', this.target).length > 0;
        if (isRelicPage) {
            this.logText("On TS Relic Page");
            // The game loads blank data (a dash) with this page load. Three websocket messages later, the real data comes in.
            // Wait for that.
            window.setTimeout(function() {MQO_DataScraper.scrapeDataRelics(MQO_DataScraper.target);}, 250);
            return;
        }

        let isFocusPage = jQuery('button#btnFocus0', this.target).length > 0;
        if (isFocusPage) {
            this.logText("On TS Focus Page");
            this.scrapeDataFocus(this.target);
            return;
        }

        let isAccountPage = jQuery('button#btnLogout', this.target).length > 0;
        if (isAccountPage) {
            this.logText("On Account Page");
            this.scrapeDataAccount(this.target);
            return;
        }

        let isGemPage = jQuery('button#btnSwitch1', this.target).length > 0;
        if (isGemPage) {
            this.logText("On Account Gem Boosts Page");
            this.scrapeDataGems(this.target);
            return;
        }

        let isPerkPage = jQuery("div:contains('Unlocked Perks')").length > 0;
        if (isPerkPage) {
            this.logText("On Perks Page");
            this.scrapeDataPerks(this.target);
            return;
        }

        let isIslandPage = jQuery("div:contains('Island Information')").length > 0;
        if (isIslandPage) {
            this.logText("On Island Page");
            this.scrapeDataIsland(this.target);
            return;
        }

        let isKingdomPage = jQuery("div#SubScreenChallenge").length > 0;
        if (isKingdomPage) {
            this.logText("On Kingdom Page");
            this.scrapeDataKingdom(this.target);
            return;
        }

        this.logText("Not on a page to scrape data from.");
    },
    start: function () {
        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                if (mutation.addedNodes.length > 0) {
                    MQO_DataScraper.checkAndScrape();
                }
            });
        });
        observer.observe(this.target, this.config);
    }
};
MQO_DataScraper.start();
if (unsafeWindow.MQO_DataScraper === undefined) {
    unsafeWindow.MQO_DataScraper = MQO_DataScraper;
}
