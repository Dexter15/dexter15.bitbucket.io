// ==UserScript==
// @name         MidenQuest - Princess Finder
// @namespace    dex.mqo
// @version      1.3
// @description  Automates the princess hunt, on through finding the princess. Move to 641, 455 (or change the starting coordinates in the script), make sure no action is running, then type MQO_PrincessFinder.start() into the JavaScript console. (There are still a couple of rough edges; some robustness that could be added yet, but probably won't.)
// @author       Dex
// @include      http://www.midenquest.com/Game.aspx
// @include      http://midenquest.com/Game.aspx
// ==/UserScript==

var MQO_PrincessFinder = {
    logging: true,
    initial_coords: [[600, 600], [800, 800], [800, 200], [200, 800], [200, 200], [400, 400], [600, 1], [999, 400], [400, 999], [1, 600]],
    ic_idx: -1,
    startX: 641,
    startY: 455,
    distance: 5,
    // 1 = left, 4 = right, 2 = up-left, 5 = down-right, 3 = up-right, 6 = down-left
    direction: 1,
    princessDirection: 1,
    rangeType: 0,
    state: "INTL",
    last_action: "SEARCH",
    x: this.startX,
    y: this.startY,
    logText(text, objectData=null) {
        if (this.logging) {
            console.log("dex.mqo.PrincessFinder    " + text);

            if (objectData !== null) {
                console.log(objectData);
            }
        }
    },
    initValues() {
        this.ic_idx = -1;
        this.x = this.startX;
        this.y = this.startY;
        this.distance = 5;
        this.direction = 1;
        this.princessDirection = 1;
        this.rangeType = 0;
        this.state = "INTL";
        this.last_action = "SEARCH";
    },
    move() {
        this.last_action = "MOVE";
        sendRequestContentFill('getMap.aspx?NewX=' + this.x + '&NewY=' + this.y + '&zoom=4');
    },
    search() {
        this.last_action = "SEARCH";
        sendRequestContentFill('getSearch.aspx?clue=1&null=');
    },
    start() {
        // Start finding the princess. Jump around the initial coordinates until clues for the princess are found.
        this.logText("Starting initial princess hunting.");

        // Create a promise to return to the caller (if there is one) so that they can trigger something when this script exits with a success (the princess was found),
        // or a failure (something unexpected happened, or a captcha came up).
        // The websocket library uses a callback instead, so our promise must convert the callback concept into a promise concept. This means that all callbacks must be
        // defined within this promise, or we lose the exception handling wrapping feature.
        // That in turn means our starting logic (that has to be run after the callback is in place) must also be in the promise.
        // We don't want to place the actual hunt logic in the promise because that is just massive, but we can't let it be called directly, from outside the promise
        // (as it used to be, because that was simpler and more straightforard), because then we lose the reference to resolve the promise (and again lose the error
        // wrapping feature). Also, the callback will only call a method with the "event" parameter data, and we need our hunt method to also get the promise methods
        // so it can resolve it. That means we have to the callback conversion must be a wrapper that includes our hunt method call too (instead of the conversion
        // just being the hunt method). 
        // That hunt method needs to also remove the callback when it resovles the promise, so it also needs a reference to the callback wrapper passed to it, since it
        // can't just reference itself anymore and doesn't otherwise know about this wrapper object.
        return new Promise((resolve, reject) => {
            let callbackWrapper = (event) => {
                this.hunt(event, resolve, reject, callbackWrapper);
            }
            MQO_WebsocketWrapper.addCallback(callbackWrapper, this);

            // Reset the data back to the starting values, in case this script is being run twice "in a row" (before the page is reloaded/refreshed).
            this.initValues();
            this.search();
        });
    },
    isCaptcha(event) {
        if (event.data == "CAPTCHA_CHALLENGE|1") {
            this.logText("Captcha encountered. Exiting");
            if (!this.logging) {
                // Skip the logger and log this directly because I don't have logging levels implemented and this needs to be known regardless of the logger being off.
                console.log("dex.mqo.PrincessFinder    Captcha encountered. Exiting.");
            }
            // The captcha isn't going to be solved by this script (I looked into it though).
            // Return true so any logic going on right can be stopped.
            return true;
        }
        return false;
    },
    hunt(event, resolve, reject, callbackWrapper) {
        if (this.isCaptcha(event)) {
            let success = MQO_WebsocketWrapper.removeCallback(callbackWrapper);
            reject("Captcha");
        }

        if (event.data.startsWith("LOADPAGE|")) {
            if (this.last_action === "SEARCH") {
                if (event.data.includes("You couldn't find a clue, maybe if you search more ?")) {
                    // Set the just-performed action back to MOVE, so that the script will search again when the cooldown is done.
                    this.last_action = "MOVE";
                } else {
                    let newRangeType = this.getRangeType(event.data);
                    if (newRangeType === -2) {
                        let success = MQO_WebsocketWrapper.removeCallback(callbackWrapper);
                        this.logText("Removed websocket hook: " + success);
                        reject("Unexpected Search Text.");
                    }

                    if (this.state === "INTL") {
                        if (newRangeType === 0) {
                            this.ic_idx += 1;
                            // Check for running of preset coordinates to check.
                            // if this.ic_idx < this.initial_coords.size
                            this.x = this.initial_coords[this.ic_idx][0];
                            this.y = this.initial_coords[this.ic_idx][1];
                            // else remove hook, exit
                        } else if (newRangeType > 0) {
                            this.state = "1ST_EDGE";
                            this.logText("Found the general area of the princess. Now to find the first edge. x: " + this.x + "; y: " + this.y);

                            // Perform setup
                            this.rangeType = newRangeType;
                            this.setDistanceFromRangeType();

                            if (this.x >= this.distance * 2) {
                                this.direction = 1;
                            } else {
                                this.direction = 4;
                            }
                            // Make the first edge-seeking move.
                            this.moveDistance();
                        }
                    } else if (this.state === "1ST_EDGE" || this.state === "2ND_EDGE") {
                        if (this.rangeType !== newRangeType) {
                            if (this.distance !== 1) {
                                // Went to far, turn around and start going back the other way, but in smaller steps.
                                this.rangeType = newRangeType;
                                this.flipDirection();
                            } else {
                                // We were already moving one hex at a time and just crossed a range border. We have found the first edge.
                                // We may be "outside" the first edge now though, move to the "inside" if we are not already.
                                if (this.rangeType > newRangeType) {
                                    this.flipDirection();
                                    this.moveDistance();
                                } else {
                                    this.rangeType = newRangeType;
                                }

                                if (this.state === "1ST_EDGE") {
                                    this.logText("Found the first edge border, now to figure out which direction the edge is going. x: " + this.x + "; y: " + this.y);

                                    this.princessDirection = this.direction;
                                    this.state = "1ST_EDGE_CHECK";
                                    // If moving left(1), then this edge either goes up-left/down-right or up-right/down-left. Moving left puts us in the same range no matter what, so move to up-right(3) or down-right(5) to determine which way this edge goes.
                                    // If moving right(4), then this edge either goes up-right/down-left or up-left/down-right. Moving right puts us in the same range no matter what, so move to up-left(2) or down-left(6) to determine which way this edge goes.
                                    this.direction += 2;    // No matter which way we were going, this puts us in a direction we want to try now.
                                } else if (this.state === "2ND_EDGE") {
                                    this.logText("Found the second edge border, now to jump to the princess. x: " + this.x + "; y: " + this.y);

                                    this.setDistanceFromRangeType();
                                    if (this.distance === 200) {
                                        // There is a bug in the game where the 200-range clue has the princess only 199 tiles away (instead of exactly 100, or 25, etc. for the other clues).
                                        this.distance = 199;
                                    }

                                    // 1 = left, 4 = right, 2 = up-left, 5 = down-right, 3 = up-right, 6 = down-left
                                    if (this.princessDirection === 1) {
                                        if (this.direction === 2 || this.direction === 6) {
                                            this.direction = 1;
                                        } else if (this.direction === 3) {
                                            this.direction = 2;
                                        } else if (this.direction === 5) {
                                            this.direction = 6;
                                        }
                                    } else if (this.princessDirection === 4) {
                                        if (this.direction === 2) {
                                            this.direction = 3;
                                        } else if (this.direction === 3 || this.direction === 5) {
                                            this.direction = 4;
                                        } else if (this.direction === 6) {
                                            this.direction = 5;
                                        }
                                    }
                                    this.logText("Princess direction: " + this.princessDirection + "; New direction: " + this.direction + "; Distance: " + this.distance);
                                    // This state isn't used anywhere, but it keeps the main logic flow (the move and follow-up search) going, without triggering any
                                    // of these further hunting steps.
                                    this.state = "DONE";
                                }
                            }
                        }
                        this.moveDistance();
                    } else if (this.state === "1ST_EDGE_CHECK") {
                        if (this.rangeType !== newRangeType) {
                            // This edge does not go the direction we tried. Turn around and go back to the edge we found, then set to the direction to one of the two we now know it is.
                            this.flipDirection();
                            this.moveDistance();

                            // Because of the directions tried, we now know the edge must go up-left(2)/down-right(5), no matter where we came from.
                            // Also make sure there is enough room to move in the chosen direction.
                            this.setDistanceFromRangeType();
                            if (this.y >= this.distance * 2) {
                                this.direction = 2;
                            } else {
                                this.direction = 5;
                            }
                        } else {
                            this.setDistanceFromRangeType();
                            // The edge goes the direction we checked. Keep going this way, unless we don't have room. In that case, flip around and go the opposite direction.
                            if ((this.direction === 3 && (this.x + (this.distance * 2) >= 1000 || this.y < this.distance * 2))
                                    || (this.direction === 6 && (this.x < this.distance * 2 || this.y + (this.distance * 2) >= 1000))) {
                                this.flipDirection();
                            }
                        }
                        this.state = "2ND_EDGE";
                        this.logText("Found the edge direction. direction: " + this.direction);
                    }
                }
            }
        } else if (event.data.startsWith("SETCD|0|")) {
            if (this.last_action === "MOVE") {
                this.search();
            } else if (this.last_action === "SEARCH") {
                this.move();
            } else if (this.last_action === "DONE") {
                // The hunt is over and any remaining cooldown has finished. Clean up the callback and resolve the promise.
                let success = MQO_WebsocketWrapper.removeCallback(callbackWrapper);
                this.logText("Cooldown complete. Resolving promise. Removed websocket hook: " + success);
                resolve(undefined);
            }
        } else if (event.data.startsWith("CHATNEW|") && event.data.includes("has found the lost princess")) {
            // One way or another, the princess has been found (maybe by somebody else).
            this.logText("Princess found. Waiting for cooldown to end.");
            // The is probably still a cooldown going on out there. Leave the callback in place so the promise can be resolved after that is done.
            // Can't use the state as a check, because that would stop the move and search actions at the end of a hunt (before this message appears),
            // so set a new last action that can be triggered off of to know that the script is done done.
            this.last_action = "DONE";
        }
    },
    getRangeType(html) {
        if (html.includes("You asked everyone around and nobody has seen the princess")) {
            return 0;
        } else if (html.includes("Your keen scouting senses found something")) {
            return 1;
        } else if (html.includes("After asking around, a traveler notified you that he saw her a while ago")) {
            return 2;
        } else if (html.includes("Someone saw the princess an hour ago")) {
            return 3;
        } else if (html.includes("You found fresh traces of the princess")) {
            return 4;
        } else if (html.includes("You found the princess !")) {
            return -1;
        }

        this.logText("Didn't find any of the expected search text. Aborting.");
        return -2;
    },
    setDistanceFromRangeType() {
        if (this.rangeType === 1) {
            this.distance = 200;
        } else if (this.rangeType === 2) {
            this.distance = 100;
        } else if (this.rangeType === 3) {
            this.distance = 25;
        } else if (this.rangeType === 4) {
            this.distance = 10;
        }
    },
    moveDistance() {
        // 1 = left, 4 = right, 2 = up-left, 5 = down-right, 3 = up-right, 6 = down-left
        if (this.direction === 1) {
            this.x -= this.distance;
        } else if (this.direction === 4) {
            this.x += this.distance;
        } else if (this.direction === 2) {
            this.y -= this.distance;
        } else if (this.direction === 5) {
            this.y += this.distance;
        } else if (this.direction === 3) {
            this.x += this.distance;
            this.y -= this.distance;
        } else if (this.direction === 6) {
            this.x -= this.distance;
            this.y += this.distance;
        }
    },
    flipDirection() {
        this.direction = (this.direction + 3) % 6;
        if (this.direction === 0) {
            this.direction = 6;
        }

        this.distance = Math.trunc(this.distance / 2);
        if (this.distance === 0) {
            this.distance = 1;
        }
    },
}

if (unsafeWindow.MQO_PrincessFinder === undefined) {
    unsafeWindow.MQO_PrincessFinder = MQO_PrincessFinder;
}