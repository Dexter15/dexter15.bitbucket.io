// ==UserScript==
// @name         MidenQuest - Rune Helper
// @namespace    dex.mqo
// @version      2.0
// @description  Adds a button to the rune page to auto-upgrade a rune. Select the rune you want to upgrade in the left box and then click the "upgrade" button.
// @author       Dex
// @match        http://www.midenquest.com/Game.aspx
// @match        http://midenquest.com/Game.aspx
// @downloadURL  https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest - Rune Helper.user.js
// @updateURL    https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest - Rune Helper.user.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Configurable data.
    let logging = true;

    // Static data.
    let rgxLevel = / lv. (\d+)/i;

    // Internal data.
    let queuedRuneList = [];
    let runeList = new Map();
    let working = false;

    // Methods
    function logText(text, objectData=null) {
        if (logging) {
            console.log("dex.gos.rune_helper    " + text);

            if (objectData !== null) {
                console.log(objectData);
            }
        }
    }

    function isCaptcha(event) {
        if (event.data == "CAPTCHA_CHALLENGE|1") {
            logText("Captcha encountered. Exiting.");
            // The captcha isn't going to be solved by this script (I looked into it though).
            // Return true so any logic going on right now can be stopped.
            return true;
        }
        return false;
    }

    function upgradeSelectedRune() {
        logText("Fetching selected rune.");
        working = true;
        jQuery("#dex_rune_upgrade_btn").text("Upgrading...").prop("disabled", true);

        let startingRune = jQuery("select#SelectItemSource option:selected");

        let result = rgxLevel.exec(startingRune.text());
        if (result === null) {
            logText("Could not determine starting rune level. Exiting.");
            return;
        }

        // Clear/reset the internal variables. If this is being run again without reloading the game, they will still be in the state the last rune ended with.
        queuedRuneList = [];
        runeList = new Map();

        let curLvl = parseInt(result[1], 10);
        let id = startingRune.get(0).value;
        queuedRuneList.push({id: id, lvl: curLvl});
        logText("Starting rune info: id: " + id + "; level: " + curLvl);

        loadRuneList();
        upgradeRune();
    }

    function loadRuneList() {
        logText("Loading/caching list of runes.");
        // Get the select object.
        let listbox = jQuery("select#SelectItemSource");

        let skip = false;
        listbox.find('option').each(
            function(index, element) {
                //logText("Source option index: " + index + "; element value: " + element.value + "; element text: " + element.text);

                if (element.text == "---Rune to upgrade---") {
                    // Ignore all of the runes for a while because they are the equipped runes. Keep skipping the runes until we see the
                    // option/label that denotes the "real" runes are starting next.
                    skip = true;
                    return true;
                }

                if (skip) {
                    if (element.text == "---Inventory Runes---") {
                        skip = false;
                    }
                    return true;
                }

                let result = rgxLevel.exec(element.text);
                if (result != null) {
                    let lvl = parseInt(result[1], 10);
                    let id = element.value;

                    if (runeList.has(lvl)) {
                        // There are already known runes for this level. Add this new rune to the list.
                        runeList.get(lvl).push(id);
                    } else {
                        // There are no known runes for this level. Add a new list to the map with this one rune.
                        runeList.set(lvl, [id]);
                    }
                } else {
                    // This entry does not have a value associated with it. It is probably one of the section headers or spacer lines.
                }
            }
        );
        logText("Loaded list of runes.", runeList);
    }

    function upgradeRune() {
        let curRune = queuedRuneList[queuedRuneList.length - 1];

        // If current rune id is null, see if one is in the cache (now, a previous merger may have created this rune).
        // If so, remove from cache and update this queue entry with the id.
        if (curRune.id === null) {
            logText("The next rune in the queue didn't exist before. A previous merge should have created it. Checking for that rune now. Level of currently queued rune: " + curRune.lvl);
            if (runeList.has(curRune.lvl)) {
                curRune.id = getRuneListIdForLevel(curRune.lvl);
                logText("Found a rune to use for this queued level. Rune id: " + curRune.id);
            } else {
                // The needed rune still doesn't exist, that means a previous run didn't find one or create one. There isn't
                // anything more this run can do. Exit.
                logText("Did not find a rune to use for this queued level. Exiting");
                exitPrep();
                return;
            }
        }

        // One way or another, we have the first rune (it was either found in a previous run or created via a previous run). Try to find the second.
        let desiredLvl = 0;
        if (curRune.lvl > 6) {
            desiredLvl = getDesiredLevel(curRune.lvl, 5);
        } else {
            // Use special second rune level rules if current rune level is 6 or lower, because of efficiencies of combining same level runes
            // at this level.
            desiredLvl = curRune.lvl;
        }

        logText("Current rune id: " + curRune.id + "; Current rune level: " + curRune.lvl + "; Looking for a rune of level: " + desiredLvl);
        if (runeList.has(desiredLvl)) {
            // Found a matching second rune ready to go.
            // Remove the involved runes from the tracked lists.
            let secondRuneId = getRuneListIdForLevel(desiredLvl);
            queuedRuneList.pop();
            logText("Found an existing rune of the appropriate level to merge with. Second rune id: " + secondRuneId);

            // If there are more rune merges queued up after this one, they should depend on the result of this merge.
            // Add the callback method to handle that.
            if (queuedRuneList.length > 0) {
                MQO_WebsocketWrapper.addCallback(mergeResults);
                logText("There are more runes in the queue after this one. Processing should continue, using the results of this merge.");
            } else {
                logText("The queue is now empty. This should be the final merge to get the original desired rune upgraded.");
                exitPrep();
            }

            // Send the command to merge the runes.
            sendRequestContentFill('getRuneUpgrade.aspx?up=' + curRune.id + '&ma=' + secondRuneId + '&null=');
        } else {
            logText("Did not find an existing rune of the appropriate level to merge with. Trying to find the next available rune to use, queueing up the in-between runes.");
            // Loop looking for decreasing levels until one is found or the rune list runs out.
            if (desiredLvl > 7) {
                desiredLvl = getDesiredLevel(desiredLvl, 1);
            } else {
                // Use special rune level rules if current rune level is 7 or lower, because of efficiencies of combining same level runes
                // at this level.
                desiredLvl = getDesiredLevel(desiredLvl, 2);
            }
            if (!populateQueue(desiredLvl)) {
                // We still didn't find an appropriate level rune. We have effectively run out of runes.
                // Exit upgrading until the player has found more lower level runes to build up from.
                logText("Was unable to find a next available rune to use. Exiting.");
                exitPrep();
                return;
            }
            logText("Found an available rune to use somewhere in the chain. Starting again with that rune. New rune queue:", queuedRuneList);

            // A rune was found. Start the process over again for that rune.
            upgradeRune();
        }
    }

    // Method that will handle the results of the merge.
    function mergeResults(event) {
        if (isCaptcha(event)) {
            // Clean up existing callbacks.
            MQO_WebsocketWrapper.removeCallback(mergeResults);
            exitPrep();
            return;
        }

        if (event.data.startsWith("LOADPAGE|")) {
            // Found the message we were looking for. Make this doesn't run again for later messages.
            MQO_WebsocketWrapper.removeCallback(mergeResults);

            // Get the new rune level and id and add it to the cache.
            let option = document.querySelector("select#SelectItemSource option:checked");

            let result = rgxLevel.exec(option.text);
            if (result != null) {
                let lvl = parseInt(result[1], 10);
                let id = option.value;

                // If the merge failed for any reason (i.e. using the same rune id twice, running out gems), there is no error and the
                // original rune is placed back in the results slot, causing this code to find that same rune again and try to act on
                // it again, endlessly.
                let curRune = queuedRuneList[queuedRuneList.length - 1];
                if (curRune.id == id) {
                    logText("Found the new rune data, but it's the same as what was already in the queue. That means there wasn't a new rune created (maybe the player has run out of gems, or maybe a mistake in the script tried to merge the same with itself). Exiting. New rune id: " + id + "; New rune level: " + lvl);
                    exitPrep();
                }

                if (runeList.has(lvl)) {
                    // There are already known runes for this level. Add this new(?) rune to the list.
                    // If the merge failed for any reason (i.e. using the same rune id twice, running out gems), there is no error and the
                    // original rune is placed back in the results slot, causing this code to find that same rune again and try to act on
                    // it again, endlessly.
                    if (runeList.get(lvl).includes(id)) {
                        logText("Found the new rune data, but it's the same as what was already in the cache. That means there wasn't a new rune created (maybe the player has run out of gems, or maybe a mistake in the script tried to merge the same with itself). Exiting. New rune id: " + id + "; New rune level: " + lvl);
                        exitPrep();
                    } else {
                        runeList.get(lvl).push(id);
                    }
                } else {
                    // There are no known runes for this level. Add a new list to the map with this one rune.
                    runeList.set(lvl, [id]);
                }
                logText("Found the new rune data. Added to the rune cache. New rune id: " + id + "; New rune level: " + lvl);

                // With this new rune existing and recorded in the cache, call the upgrade function again.
                logText("Starting again with the new rune.");
                upgradeRune();
            } else {
                logText("Did not find the new rune data. Exiting.");
                exitPrep();
            }
        }
    }

    function getDesiredLevel(curLvl, modifier) {
        let desiredLvl = curLvl - modifier;
        if (desiredLvl < 1) {
            desiredLvl = 1;
        }
        return desiredLvl;
    }

    function getRuneListIdForLevel(level) {
        let lvlRuneList = runeList.get(level);
        let id = lvlRuneList.pop();

        if (lvlRuneList.length < 1) {
            runeList.delete(level);
        }

        return id;
    }

    function populateQueue(desiredLvl) {
        // Loop looking for decreasing levels until one is found or the rune list runs out.
        let stop = false;
        for (; desiredLvl > 0 && !stop; desiredLvl--) {
            logText("Looping. desiredLvl: " + desiredLvl);
            if (runeList.has(desiredLvl)) {
                stop = true;
                let tempRuneId = getRuneListIdForLevel(desiredLvl);
                queuedRuneList.push({id: tempRuneId, lvl: desiredLvl});
            } else {
                queuedRuneList.push({id: null, lvl: desiredLvl});

                if (desiredLvl < 8) {
                    // Start decrementing by twos, because of the efficiency of combining same level runes this low.
                    // This will end up skipping the 2+2, 4+4 and 6+6 combinations here, but those levels of runes should be used during
                    // higher level upgrades (e.g. upgrading a level 7 rune). Players will most often run out of the low even level runes
                    // before they rune out of level 1 runes. (And if we didn't skip the evens, this loop would end up ending at level 2
                    // and never check the level 1 runes.)
                    desiredLvl--;
                }
            }
        }

        if (!stop) {
            // We still didn't find an appropriate level rune. We have effectively run out of runes.
            return false;
        }

        // There was an available rune found and added to the queue (in addition to all the in-between runes being added to the queue).
        return true;
    }

    function exitPrep() {
        working = false;
    }

    // Initialize
    let observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            if (mutation.addedNodes.length > 0) {
                let hasRuneSelect = jQuery('select#SelectItemSource', document.getElementById('ContentLoad')).length;
                if (hasRuneSelect > 0) {
                    logText("On Rune Page. Adding buttons.");
                    jQuery("#ContentLoad > div:nth-child(" + 3 + ")").append("<div id='dex_rune_helper_div'></div>");
                    let runeHelperDiv = jQuery("#dex_rune_helper_div");

                    runeHelperDiv.append('<button id="dex_rune_upgrade_btn" class="darkBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 9pt;" role="button" aria-disabled="false"><span class="ui-button-text">Upgrade</span></button>');

                    if (working) {
                        jQuery("#dex_rune_upgrade_btn").text("Upgrading...").prop("disabled", true);
                    } else {
                        jQuery("#dex_rune_upgrade_btn").click(function (event) {upgradeSelectedRune()});
                    }
                }
            }
        });
    });
    observer.observe(document.getElementById('ContentLoad'), {childList: true});
})();