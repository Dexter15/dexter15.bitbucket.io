// ==UserScript==
// @name         MidenQuest - MQO Utility Alternate
// @namespace    dex.mqo
// @version      1.0.0
// @description  Adds an action bar to the top of the game to automate various tasks. Originally created by Dave McClelland <davidmcclelland@gmail.com> - https://github.com/davidmcclelland/MqoUtility. This version rewrites the script to use the game's websockets and to fit some of my personal tastes instead (dropping some things, adding some things, making some things work in a bit of a different way).
// @author       Dex
// @match        http://www.midenquest.com/Game.aspx
// @match        http://midenquest.com/Game.aspx
// @grant        none
// @downloadURL  https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest - MQO Utility Alternate.user.js
// @updateURL    https://dexter15.bitbucket.io/MQO/tampermonkey/MidenQuest - MQO Utility Alternate.user.js
// ==/UserScript==


let MQO_Auto_Toolbar = {
    // Configurable data.
    perkMinDays: 5.2,
    perksActive: ["drunk", "enraged", "lucky", "hoarder"],    // Uses the keys of perkIds.
    perkRsrc: "plant",    // Uses the keys of perkRsrcIds.
    logging: true,
    // This is used after the princess is found, if that script is installed.
    home: {x: 641, y: 455, skillNumber: 4},    // skillNumbers determined by the game code. 1 = ?, 2 = ?, 3 = ?, 4 = Gathering, ...

    // Static data.
    dealIds: [{btnId: "#btnBuy1"}, {btnId: "#btnBuy2"}, {btnId: "#btnBuy3"}, {btnId: "#btnBuy4"}, {btnId: "#btnBuy5"}],
    chestIds: [{btnId: "#btnOpenChest4", numKeys: 10}, {btnId: "#btnOpenChest3", numKeys: 5}, {btnId: "#btnOpenChest2", numKeys: 1}],
    bagIds: [{btnId: "#btnOpenResBag"}, {btnId: "#btnOpen"}],
    perkIds: {drunk: {btnId: "#btnActivatePerk21"}, enraged: {btnId: "#btnActivatePerk22"}, lucky: {btnId: "#btnActivatePerk23"}, pitied: {btnId: "#btnActivatePerk24"}, hoarder: {btnId: "#btnActivatePerk25"}, captain: {btnId: "#btnActivatePerk26"}},
    perkRsrcIds: {ore: {btnId: "#btnActivateOre"}, plant: {btnId: "#btnActivatePlant"}, wood: {btnId: "#btnActivateWood"}, fish: {btnId: "#btnActivateFish"}},

    // Methods
    logText(text, objectData=null) {
        if (this.logging) {
            console.log("dex.mqo.autoToolbar    " + text);

            if (objectData !== null) {
                console.log(objectData);
            }
        }
    },
    isUsable: function(elId) {
        return elId.length && (elId[0].style.display !== 'none');
    },
    isCaptcha(event) {
        if (event.data == "CAPTCHA_CHALLENGE|1") {
            this.logText("Captcha encountered. Exiting.");
            // The captcha isn't going to be solved by this script (I looked into it though).
            // Return true so any logic going on right now can be stopped.
            return true;
        }
        return false;
    },
    displayToolbar: function() {
        let autoToolbar = this;
        $('#ZoneContent').css("height", "580px");
        $('#ZoneOptions').css("height", "582px");
        $('#TitleEmbedded').after(this.toolbarHtml);
        $('#auto_toolbar_dealBuyer').on("click", $.proxy(autoToolbar.buyDeals, autoToolbar));
        $('#auto_toolbar_chestOpener').on("click", $.proxy(autoToolbar.openAllChests, autoToolbar));
        $('#auto_toolbar_bagOpener').on("click", $.proxy(autoToolbar.openBagsAndKeys, autoToolbar));
        $('#auto_toolbar_allOpener').on("click", $.proxy(autoToolbar.openAll, autoToolbar));
        $('#auto_toolbar_bulkSell').on("click", $.proxy(autoToolbar.bulkSell, autoToolbar));
        $('#auto_toolbar_mntnKngdm').on("click", $.proxy(autoToolbar.maintainKingdom, autoToolbar));
        $('#auto_toolbar_setPerkTimers').on("click", $.proxy(autoToolbar.setPerkTimers, autoToolbar));
        $('#auto_toolbar_runePage').on("click", $.proxy(autoToolbar.loadRunePage, autoToolbar));

        if (typeof MQO_PrincessFinder !== 'undefined') {
            this.logText("Adding button to start Princess Finder.");
            $("#auto_toolbar_runePage").after(this.princessBtn);
            $("#auto_toolbar_princessFinder").on("click", $.proxy(autoToolbar.princessBtnClick, autoToolbar));
        }
    },
    buyDeals: function() {
        this.logText("Buying island market deals.");
        MQO_WebsocketWrapper.addCallback(this.buyingDeals, this);
        sendRequestContentFill('getNavigation.aspx?screen=2&market=1');
    },
    buyingDeals: function(event) {
        this.logText("Processing page/deals results.", event);
        if (this.isCaptcha(event)) {
            // Reset the state back to the beginning (so it doesn't skip stuff if run again) and clean up existing callbacks.
            MQO_WebsocketWrapper.removeCallback(this.buyingDeals);

            if (this.dealsPromiseReject) {
                // There is something waiting for this to finish to continue (the "open all" button); reject the promise so that it doesn't run the next step.
                this.dealsPromiseReject("Captcha");
                this.dealsPromiseResolve = null;
                this.dealsPromiseReject = null;
            }

            return;
        }

        if (event.data.startsWith("LOADPAGE|") && event.data.includes("Marketplace")) {
            // This handles the initial loading of the page and the subsequent purchases.
            let found = this.openItem(false, this.dealIds);

            if (!found) {
                // Nothing more to click, remove this listener.
                MQO_WebsocketWrapper.removeCallback(this.buyingDeals);

                if (this.dealsPromiseResolve) {
                    // There is something waiting for this to finish to continue (the "open all" button); resolve the promise so that that can run the next step.
                    this.dealsPromiseResolve(undefined);
                    this.dealsPromiseResolve = null;
                    this.dealsPromiseReject = null;
                }
            }
        }
    },
    openAllChests: function() {
        this.logText("Opening Gambler's chests.");
        MQO_WebsocketWrapper.addCallback(this.openingChests, this);
        sendRequestContentFill('getNavigation.aspx?screen=2&gambling=1');
    },
    openingChests: function(event) {
        this.logText("Processing page/chest results.");
        if (this.isCaptcha(event)) {
            // Reset the state back to the beginning (so it doesn't skip stuff if run again) and clean up existing callbacks.
            MQO_WebsocketWrapper.removeCallback(this.openingChests);

            if (this.chestPromiseReject) {
                // There is something waiting for this to finish to continue (the "open all" button); reject the promise so that it doesn't run the next step.
                this.chestPromiseReject("Captcha");
                this.chestPromiseResolve = null;
                this.chestPromiseReject = null;
            }

            return;
        }

        // This handles the initial loading of the page, and the results of opening the chest, as the same LOADPAGE response comes back in either case, but with a small, hard to confirm message added with the chest results.
        if (event.data.startsWith("LOADPAGE|") && event.data.includes("Gambler's Post")) {
            let found = this.openItem(true, this.chestIds);

            if (!found) {
                // Nothing more to click, remove this listener.
                MQO_WebsocketWrapper.removeCallback(this.openingChests);

                if (this.chestPromiseResolve) {
                    // There is something waiting for this to finish to continue (the "open all" button); resolve the promise so that that can run the next step.
                    this.chestPromiseResolve(undefined);
                    this.chestPromiseResolve = null;
                    this.chestPromiseReject = null;
                }
            }
        }
    },
    openBagsAndKeys: function() {
        this.logText("Opening bags and keys.");
        MQO_WebsocketWrapper.addCallback(this.openingBagsAndKeys, this);
        // Fake a successful open event so that this function will try to open a bag/key (we need to open one to get started).
        let event = {data: "OPENR|"};
        this.openingBagsAndKeys(event);
    },
    openingBagsAndKeys: function(event) {
        this.logText("Processing bag/key results.");
        if (this.isCaptcha(event)) {
            // Reset the state back to the beginning (so it doesn't skip stuff if run again) and clean up existing callbacks.
            MQO_WebsocketWrapper.removeCallback(this.openingBagsAndKeys);

            return;
        }

        if (event.data.startsWith("OPENR|")) {
            let found = this.openItem(false, this.bagIds);

            if (!found) {
                // Nothing more to click, remove this listener.
                MQO_WebsocketWrapper.removeCallback(this.openingBagsAndKeys);
                // Close the dialog.
                $("#ui-dialog-title-dialogChest").next().click();
            }
        }
    },
    openAll: function() {
        this.logText("Buying island market deals, opening Gambler's chests, and opening bags/keys.");
        (new Promise((resolve, reject) => {
            this.dealsPromiseResolve = resolve;
            this.dealsPromiseReject = reject;
            this.buyDeals();
        })).then(result => {
            return new Promise((resolve, reject) => {
                this.chestPromiseResolve = resolve;
                this.chestPromiseReject = reject;
                this.openAllChests();
            });
        }).then(result => {
            this.openBagsAndKeys();
        });
    },
    setPerkTimers: function() {
        this.logText("Setting the configured perks to the configured minimum time using the configured resource.");
        let curPerk = 0;
        let handlePerkPage, handlePerkType;

        handlePerkPage = (event) => {
            if (this.isCaptcha(event)) {
                // Clean up existing callbacks.
                MQO_WebsocketWrapper.removeCallback(handlePerkPage);
                return;
            }

            if (event.data.startsWith("LOADPAGE|")) {
                MQO_WebsocketWrapper.removeCallback(handlePerkPage);

                let perkTimers = this.getPerkTimersString(event);
                if (perkTimers === null) {
                    // Problem. Abort.
                    return;
                }

                // Find the next perk to increase the time for.
                curPerk = this.getNextPerk(curPerk, perkTimers);
                if (curPerk === -1) {
                    // Ran out of perks to increase. Everything is at the desired length (or longer). Simply exit.
                    return;
                }

                let btnFound = false;
                MQO_WebsocketWrapper.addCallback(handlePerkType, this);
                while (!btnFound && this.perksActive.length > curPerk) {

                    // Wrap the object in an array so it is compatible with the generic button/open function.
                    let perkTypeArray = [ this.perkIds[this.perksActive[curPerk]] ];
                    btnFound = this.openItem(false, perkTypeArray);

                    if (!btnFound) {
                        // Uh, the script has been misconfigured. The user doesn't have the perk specified. Move on to the next one.
                        curPerk = this.getNextPerk(curPerk, perkTimers);
                        if (curPerk === -1) {
                            // Ran out of perks to increase. Everything is at the desired length (or longer). Clean up and exit.
                            MQO_WebsocketWrapper.removeCallback(handlePerkType);
                            return;
                        }
                    }
                }

                if (!btnFound) {
                    // Ran out of perks to try. Clean up and exit.
                    MQO_WebsocketWrapper.removeCallback(handlePerkType);
                    return;
                }
            }
        }

        handlePerkType = (event) => {
            this.logText("Adding time to perk.");
            if (this.isCaptcha(event)) {
                // Clean up existing callbacks.
                MQO_WebsocketWrapper.removeCallback(handlePerkType);

                return;
            }

            if (event.data.startsWith("LOADPAGE|")) {
                // Get the latest perk time for this perk, so we can tell if previous calls to this method added enough time already. (If this is the first
                // time this callback is running for this perk, this is redundant, but it's needed on subsequent calls.)
                let timer = this.getCurrentPerkTimer(event);
                let desiredPerkMinutes = this.perkMinDays * 24 * 60;
                if (timer === -1 || timer >= desiredPerkMinutes) {
                    this.logText("Done adding time to perk.");
                    // This perk had a problem, or no longer needs time added to it. Go back to the main perk logic so it can move on to the next perk (or end if all perks are done).
                    MQO_WebsocketWrapper.removeCallback(handlePerkType);
                    // When you update a perk, the timer list does not get updated, which is what the main perk logic uses. Go back to the main perk logic by refreshing the page, so that the timer list will get updated.
                    MQO_WebsocketWrapper.addCallback(handlePerkPage, this);
                    sendRequestContentFill('getInfoPerk.aspx?null=');
                    return;
                }

                // Wrap the object in an array so it is compatible with the generic button/open function.
                let perkBtnArray = [ this.perkRsrcIds[this.perkRsrc] ];
                let btnFound = this.openItem(false, perkBtnArray);

                if (!btnFound) {
                    // The user doesn't have enough of their preferred resources left. Exit.
                    this.logText(`Exiting due to lack of preferred resource. Preferred resource: ${this.perkRsrc}`);
                    return;
                }
            }
        }

        MQO_WebsocketWrapper.addCallback(handlePerkPage, this);
        // These two are game/global level calls and variables.
        sendRequestContentFill('getInfoPerk.aspx?null=');
        fightengaged = 0;
    },
    bulkSell: function() {
        this.logText("Starting bulk sell.");
        let loopSell;

        let initialSell = (event) => {
            if (this.isCaptcha(event)) {
                // Clean up existing callbacks.
                MQO_WebsocketWrapper.removeCallback(initialSell);

                return;
            }

            if (event.data.startsWith("LOADPAGE|")) {
                this.logText("Attempting to sell first batch.");
                MQO_WebsocketWrapper.removeCallback(initialSell);
                MQO_WebsocketWrapper.addCallback(loopSell, this);
                sendRequestContentFill('getCustomize.aspx?bulk=1&bulks=15&bulkq=4&bulke=1&confirmbs=1&null=');
            }
        }

        loopSell = (event) => {
            if (this.isCaptcha(event)) {
                // Clean up existing callbacks.
                MQO_WebsocketWrapper.removeCallback(loopSell);

                return;
            }

            if (event.data.startsWith("LOADPAGE|")) {
                this.logText("Checking if there are still items to sell.");
                let rgxTimer = /Items about to get sold:[^\d]+\d+ out of (\d+) items of tier/;
                let result = rgxTimer.exec(event.data);
                if (result != null) {
                    let numItems = parseInt(result[1], 10);

                    this.logText("Items remaining: " + numItems);
                    if (numItems > 0) {
                        sendRequestContentFill('getCustomize.aspx?bulk=1&bulks=15&bulkq=4&bulke=1&confirmbs=1&null=');
                    } else {
                        MQO_WebsocketWrapper.removeCallback(loopSell);
                    }
                }
            }
        }

        MQO_WebsocketWrapper.addCallback(initialSell, this);
        sendRequestContentFill('getCustomize.aspx?bulk=1&null=');
    },
    maintainKingdom: function() {
        this.logText("Loading kingdom page.");

        let kingdomFoodLoaded = (event) => {
            if (this.isCaptcha(event)) {
                // Clean up existing callbacks.
                MQO_WebsocketWrapper.removeCallback(kingdomFoodLoaded);

                return;
            }

            if (event.data.startsWith("LOADPAGE|")) {
                this.logText("Attempting to maintain kingdom with T5 fish.");
                MQO_WebsocketWrapper.removeCallback(kingdomFoodLoaded);

                let rgxFoodAmt = /\$\("#btnMt5a3"\).click\(function \(\) { sendRequestContentFill\('getKingdom.aspx\?action=maintain&t5=(\d+)&null='\); }\);/;
                let result = rgxFoodAmt.exec(event.data);
                if (result != null) {
                    let numFood = parseInt(result[1], 10);

                    this.logText("T5 Fish amount: " + numFood);
                    if (numFood > 0) {
                        sendRequestContentFill('getKingdom.aspx?action=maintain&t5=' + numFood + '&null=');
                    }
                }

                // Does overfeeding cause the excess to just disappear into the ether?
                sendRequestContentFill('getKingdom.aspx?action=maintain&t5=99999&null=');
            }
        }

        let kingdomLoaded = (event) => {
            if (this.isCaptcha(event)) {
                // Clean up existing callbacks.
                MQO_WebsocketWrapper.removeCallback(kingdomLoaded);

                return;
            }

            if (event.data.startsWith("LOADPAGE|")) {
                this.logText("Attempting to maintain kingdom.");
                MQO_WebsocketWrapper.removeCallback(kingdomLoaded);
                MQO_WebsocketWrapper.addCallback(kingdomFoodLoaded, this);
                sendRequestContentFill('getKingdom.aspx?action=maintain&null=');
            }
        }

        MQO_WebsocketWrapper.addCallback(kingdomLoaded, this);
        sendRequestContentFill('getKingdom.aspx?null=');
    },
    loadRunePage: function() {
        sendRequestContentFill('getRuneUpgrade.aspx?id=&null=');
    },
    openItem: function(isChest, btnIds) {
        // We could open many chests/bags/keys at once here, but that could also cause some server issues, so let's just open them one at a time instead.
        // For that reason, use the "some" function so that we stop at the first pressable button, and can return a flag to indicate whether a pressable button was found (and if not, remove the listener).
        let found = btnIds.some(data => {
            // If opening a chest, make sure the player has enough keys to open it, before trying to open it (otherwise the game will return the same LOADPAGE response, causing an endless loop until the player finally has enough keys, which is not ideal).
            // This returns a float because the system commonly uses decimals (like having "1.1k" keys) and so this gives a more accurate result most of the time. If you only have 10 keys though, this could return 9.9999999 instead, causing that gold chest
            // to not be opened. I'm too lazy to add an epsilon check right now, and I don't think that will be a common occurance (though it will happen), so I'm leaving it as it is for now.
            if (!isChest || data.numKeys <= this.getFullNumber($("#SideChest").text())) {
                let btn = $(data.btnId);
                if (this.isUsable(btn)) {
                    btn.click();
                    // Only do one click/open at a time.
                    return true;
                }
            }
        });

        return found;
    },
    getFullNumber: function(shorthand) {
        let num = parseFloat(shorthand);

        if (shorthand.indexOf('k') !== -1) {
            num *= 1e3;
        } else if (shorthand.indexOf('m') !== -1) {
            num *= 1e6;
        } else if (shorthand.indexOf('b') !== -1) {
            num *= 1e9;
        } else if (shorthand.indexOf('t') !== -1) {
            num *= 1e12;
        }

        return num;
    },
    getPerkTimersString: function(event) {
        // Get the current perk timers to see if they are already at the minimum configured time.
        let start = event.data.indexOf("Active Perks");
        if (start === -1) {
            // Problem. Abort.
            this.logText("Could not find active perk information. Aborting.");
            return null;
        }
        // Move to the end of the div with this text.
        start += 18;
        let end = event.data.indexOf("</div>", start);
        let perkTimers = event.data.substring(start, end);
        this.logText("Perk timers string: " + perkTimers);

        return perkTimers;
    },
    getCurrentPerkTimer: function(event) {
        let rgxTimer = />Timer: <[^>]*>(\d+) min/;
        let result = rgxTimer.exec(event.data);
        if (result != null) {
            return parseInt(result[1], 10);
        }

        // Couldn't find a match. Problem.
        return -1;
    },
    getNextPerk: function(curPerkIndex, perkTimers) {
        let desiredPerkMinutes = this.perkMinDays * 24 * 60;

        while (curPerkIndex < this.perksActive.length) {
            this.logText("Check for current time on perk:", this.perksActive[curPerkIndex]);

            // Reset from a potential previous loop.
            let rgxPerkAndTime = /(\w+)\s\(([\d]+)\)\s/g;
            let found = false;
            let result;

            while ((result = rgxPerkAndTime.exec(perkTimers)) != null && !found) {
                let curPerk = result[1].toLowerCase();

                if (curPerk === this.perksActive[curPerkIndex]) {
                    found = true;
                    let temp = parseInt(result[2], 10);
                    if (temp >= desiredPerkMinutes) {
                        this.logText(`Current perk is already set to more than the desired length. Moving on to the next one. Perk: ${this.perksActive[curPerkIndex]}; Desired minutes: ${desiredPerkMinutes}; Current minutes: ${temp}`);
                    } else {
                        this.logText(`Current perk is not at the desired length. Adding perk time. Perk: ${this.perksActive[curPerkIndex]}; Desired minutes: ${desiredPerkMinutes}; Current minutes: ${temp}`);
                        return curPerkIndex;
                    }
                }
            }

            if (!found) {
                this.logText(`Current perk is not in the list, so it is not at the desired length. Adding perk time. Perk: ${this.perksActive[curPerkIndex]}; Desired minutes: ${desiredPerkMinutes}`);
                return curPerkIndex;
            }

            curPerkIndex++;
        }

        // Ran out of perks to increase. Everything is at the desired length (or longer). Simply exit.
        return -1;
    },
    toolbarHtml: `
<div id="auto_toolbar_buttons" style="margin-top: 0.1rem; margin-bottom: 0.1rem;">
    <button id="auto_toolbar_dealBuyer" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 8pt;">
        <span class="ui-button-text">Deals</span>
    </button>
    <button id="auto_toolbar_chestOpener" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 8pt;">
        <span class="ui-button-text">Chests</span>
    </button>
    <button id="auto_toolbar_bagOpener" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 8pt;">
        <span class="ui-button-text">Bags/Keys</span>
    </button>
    <button id="auto_toolbar_allOpener" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 8pt;">
        <span class="ui-button-text">Open All</span>
    </button>
    <button id="auto_toolbar_bulkSell" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 8pt;">
        <span class="ui-button-text">Sell</span>
    </button>
    <button id="auto_toolbar_mntnKngdm" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 8pt;">
        <span class="ui-button-text">Feed KD</span>
    </button>
    <button id="auto_toolbar_setPerkTimers" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 8pt;">
        <span class="ui-button-text">Perks</span>
    </button>
    <button id="auto_toolbar_runePage" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 8pt;">
        <span class="ui-button-text">Runes</span>
    </button>
</div>`,
    princessBtn: `
    <button id="auto_toolbar_princessFinder" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="font-size: 8pt;">
        <span class="ui-button-text">Princess</span>
    </button>`,
    princessBtnClick: function() {
        this.logText("Clicked to start finding princess, going to try to stop skilling.");
        // Declare the function variables up here, so they can be referenced in the callbacks below. They aren't defined yet, but by the time each
        // callback runs and uses these, they will be.
        let handleSkillScreenLoad, handleSkillStopped, handleBackHome;

        // Define the callback to handle the loading of the skill screen (to click the "Stop" button).
        handleSkillScreenLoad = (event) => {
            if (this.isCaptcha(event)) {
                // Clean up existing callbacks and exit.
                MQO_WebsocketWrapper.removeCallback(handleSkillScreenLoad);
                return;
            }

            if (event.data.startsWith("LOADPAGE|") && event.data.includes("id=\"btnStop\"")) {
                MQO_WebsocketWrapper.removeCallback(handleSkillScreenLoad);
                MQO_WebsocketWrapper.addCallback(handleSkillStopped, this);

                // Click to stop skilling.
                this.logText("Stopping skilling.");
                $("#btnStop").click();
            }
        }

        // Define the callback to handle when skilling has fully stopped (the cooldown is done) (which is to fire off the princess finding).
        handleSkillStopped = (event) => {
            if (this.isCaptcha(event)) {
                // Clean up existing callbacks and exit.
                MQO_WebsocketWrapper.removeCallback(handleSkillStopped);
                return;
            }

            if (event.data.startsWith("SETCD|0|")) {
                MQO_WebsocketWrapper.removeCallback(handleSkillStopped);

                this.logText("Skilling has stopped. Start the princess finding.");
                MQO_PrincessFinder.start.call(MQO_PrincessFinder).then(() => {
                    this.logText("Princess found and cooldown over. Returning to home tile.");
                    MQO_WebsocketWrapper.addCallback(handleBackHome, this);
                    sendRequestContentFill('getMap.aspx?NewX=' + this.home.x + '&NewY=' + this.home.y + '&zoom=4');
                });
            }
        }

        handleBackHome = (event) => {
            if (this.isCaptcha(event)) {
                // Clean up existing callbacks and exit.
                MQO_WebsocketWrapper.removeCallback(handleBackHome);
                return;
            }

            if (event.data.startsWith("LOADPAGE|")) {
                MQO_WebsocketWrapper.removeCallback(handleBackHome);
                this.logText("At home tile, starting skilling.");
                TradeskillStart(this.home.skillNumber);
            }
        }

        this.logText("Loading current skill page.");
        // Now that the callbacks are defined, click the action bar in order to load the skill screen, and set the callback to handle that.
        $("#prgActionOverlay").click();
        MQO_WebsocketWrapper.addCallback(handleSkillScreenLoad, this);
    }
};

MQO_Auto_Toolbar.displayToolbar();
