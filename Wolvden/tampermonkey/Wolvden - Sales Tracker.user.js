// ==UserScript==
// @name         Wolvden - Sales Tracker
// @namespace    dex.wolvden
// @version      1.1
// @description  Counts the items of a type you have for sale still, and that you have sold (since the last tracker reset).
// @author       Dex
// @match      https://www.wolvden.com/trading-center/manage
// @match      https://www.wolvden.com/inbox/read/*
// @downloadURL  https://dexter15.bitbucket.io/Wolvden/tampermonkey/Wolvden - Sales Tracker.user.js
// @updateURL    https://dexter15.bitbucket.io/Wolvden/tampermonkey/Wolvden - Sales Tracker.user.js
// ==/UserScript==

var Wolvden_SalesTracker = {
    logging: true,
    allSales: {Amusement: [],
               Food: [],
               Herb: [{description: "Aloe", price: 24, searchTerm: "Aloe"}, {description: "Arnica", price: 20, searchTerm: "Arnica"}, {description: "Bearberry", price: 14, searchTerm: "Bearberry"}, {description: "Boneset", price: 19, searchTerm: "Boneset"},
                      {description: "Buffaloberry", price: 17, searchTerm: "Buffaloberry"}, {description: "Burning Bush", price: 23, searchTerm: "Burning Bush"}, {description: "Carrionflower", price: 19, searchTerm: "Carrionflower"},
                      {description: "Cedar Bark", price: 19, searchTerm: "Cedar Bark"}, {description: "Chaparral", price: 72, searchTerm: "Chaparral"}, {description: "Charcoal", price: 39, searchTerm: "Charcoal"},
                      {description: "Dandelion", price: 37, searchTerm: "Dandelion"}, {description: "Feverfew", price: 20, searchTerm: "Feverfew"}, {description: "Garlic", price: 19, searchTerm: "Garlic"}, {description: "Ginger", price: 18, searchTerm: "Ginger"},
                      {description: "Goldenseal", price: 24, searchTerm: "Goldenseal"}, {description: "Mullein", price: 16, searchTerm: "Mullein"}, {description: "Oregano", price: 21, searchTerm: "Oregano"},
                      {description: "Redwood Sorrel", price: 22, searchTerm: "Redwood Sorrel"}, {description: "Spoonwood", price: 21, searchTerm: "Spoonwood"}, {description: "St. John's Wort", price: 22, searchTerm: "St. John's Wort"},
                      {description: "Tansy", price: 18, searchTerm: "Tansy"}, {description: "Turmeric", price: 32, searchTerm: "Turmeric"}, {description: "Yarrow", price: 22, searchTerm: "Yarrow"}],
               Medicine: [{description: "Diarrhea Cure", price: 155, searchTerm: "Diarrhea Cure"}],
               Other: [{description: "10x Large Leaf, 10x Large Branch, 10x Large Rock", price: 200, searchTerm: "leaf" }],
               Decoration: [{description: "Arctic White (2 uses)", price: 8, searchTerm: "Arctic White"}, {description: "Baltimore Checkerspot (2 uses)", price: 14, searchTerm: "Baltimore Checkerspot"}],
               Crafting: [{description: "Butterfly Wing", price: 21, searchTerm: "Butterfly Wing"}]
    },
    COMBO_DELAY: 1000,
    salesData: {
        CURRENT:{
            seen: [],
            totals: {}
        },
        SOLD: {
            seen: [],
            totals: {}
        }
    },
    SOURCE_CURRENT: "CURRENT",
    SOURCE_SOLD: "SOLD",
    countHack: 0,
    logText: function(text) {
        if (Wolvden_SalesTracker.logging) {
            console.log("dex.wolvden.salesTracker    " + text);
        }
    },
    loadData: function() {
        let salesDataString = localStorage.getItem("dex_wolvden_salesData");
        this.logText("Stringified data from storage: " + salesDataString);

        if (salesDataString != null) {
            this.salesData = JSON.parse(salesDataString);
        }
    },
    saveData: function() {
        let salesDataString = JSON.stringify(this.salesData);
        localStorage.dex_wolvden_salesData = salesDataString;
    },
    trackSale: function(source) {
        if (source === this.SOURCE_CURRENT) {
            // This page has all current data. There is no need to build upon previous data (and in fact, would produce incorrect results).
            this.salesData.CURRENT.seen = [];
            this.salesData.CURRENT.totals = {};
        }

        var els = document.querySelectorAll("a[href^='/trade/']");
        // If this is a sale notification, there will only be one results, but this works just as well.
        els.forEach((el) => {
            let link = el.getAttribute("href");
            let text = el.textContent;
            this.logText("Link: " + link + "; text: " + text);

            let tradeId = link.substring(7);
            if (this.salesData[source].seen.includes(tradeId)) {
                this.logText("Already tracked " + source + " trade. Id: " + tradeId);
            } else {
                this.logText("Untracked " + source + " trade. Id: " + tradeId);
                let category = "Other";
                let item = "";

                let i = text.indexOf(":");
                if (i !== -1) {
                    category = text.substring(0, i);
                    item = text.substring(i + 2);
                } else {
                    item = text;
                }

                i = item.indexOf("(");
                if (i !== -1) {
                    item = item.substring(0, i - 1);
                }
                this.logText("Category: " + category + "; Item to track: " + item);

                if (this.salesData[source].totals[category] == null) {
                    this.salesData[source].totals[category] = {};
                }
                let total = this.salesData[source].totals[category][item];
                if (total == null) {
                    total = 0;
                }

                this.salesData[source].totals[category][item] = ++total;
                this.salesData[source].seen.push(tradeId);
            }
        });

        this.saveData();
        console.log(this.salesData[source].totals);
    },
    clearSold: function() {

    },
    displayHud: function() {
        document.querySelector("#main > ul.nav").insertAdjacentHTML("afterend", this.hudHtml);

        let salestracker = this;
        document.getElementById('salestrackerClearSold').onclick = function() {
            salestracker.salesData.SOLD.seen = [];
            salestracker.salesData.SOLD.totals = {};
            salestracker.saveData();
            salestracker.logText("SOLD data has been cleared.");
        }

        // Must use jQuery here because show.bs.modal is not an event but a jQuery-only trigger.
        $('#salestrackerDialog').on('show.bs.modal', function () {
            salestracker.logText("Loading sales data dialog.");
            // Clear the item table contents of the dialog, because they might have changed since the last time the dialog was loaded.
            let parent = document.getElementById("salestrackerTableCol1");
            while (parent.firstChild) {
                parent.removeChild(parent.firstChild);
            }
            parent = document.getElementById("salestrackerTableCol2");
            while (parent.firstChild) {
                parent.removeChild(parent.firstChild);
            }
            parent = document.getElementById("allSalesCol1");
            while (parent.firstChild) {
                parent.removeChild(parent.firstChild);
            }

            let useCol1 = true;
            let leftSize = 0;
            let rightSize = 0;
            // Combine the categories of the items for sale and those already sold into one array, sort them alphabetically, and deduplicate them via a Set.
            let categories = new Set(Object.keys(salestracker.salesData.CURRENT.totals).concat(Object.keys(salestracker.salesData.SOLD.totals)).sort());
            categories.forEach((category) => {
                // Create the table for this category.
                let html = `
                  <h3>${category}</h3>
                  <table class='table table-striped'>
                    <thead>
                      <tr>
                        <th scope="col">Item Name</th>
                        <th scope="col">For Sale</th>
                        <th scope="col">Sold</th>
                      </tr>
                    </thead>
                    <tbody>
                      ${ salestracker.salesTableRows(category) }
                    </tbody>
                  </table>`;

                let el = {};
                if (useCol1) {
                    el = document.getElementById("salestrackerTableCol1");
                    leftSize += salestracker.countHack;

                    if (leftSize > rightSize) {
                        useCol1 = !useCol1;
                    }
                } else {
                    el = document.getElementById("salestrackerTableCol2");
                    rightSize += salestracker.countHack;

                    if (rightSize >= leftSize) {
                        useCol1 = !useCol1;
                    }
                }
                el.insertAdjacentHTML("beforeend", html);
            });

            categories = Object.keys(salestracker.allSales);
            categories.forEach((category) => {
                let salesCol1 = document.getElementById("allSalesCol1");
                let tempEl = document.createElement("h4");
                tempEl.style.marginTop = "0.5rem";
                tempEl.style.marginBottom = "0.5rem";
                tempEl.appendChild(document.createTextNode(category));
                salesCol1.appendChild(tempEl);

                salestracker.allSales[category].forEach((item, index) => {
                    let tempDiv = document.createElement("div");
                    tempEl = document.createElement("a");
                    tempEl.setAttribute("href", "javascript:void(0)");
                    tempEl.appendChild(document.createTextNode(item.description));
                    tempEl.addEventListener("click", function() {
                        // The second of these definitely gives a pop-up blocked message when run on its own (though it may not when run from a button press).
                        // The first may also, but I already allowed pop-ups so I don't know.
                        // var newTab = window.open('create', '_blank');
                        // newTab.location;
                        // or
                        // window.open('create', '_blank' );
                        for (let i = 0; i < 3; i++) {
                            let newTab = window.open('create', '_blank');
                            newTab.location;
                            newTab.addEventListener('load', function() {
                                newTab.document.getElementById("txtName").value = category + ": " + item.description;
                                newTab.document.getElementById("radTradePublic").checked = true;
                                newTab.document.getElementById("txtBuyoutSC").value = item.price + i;

                                let filterBox = newTab.document.getElementById("txtFilterItems");
                                filterBox.value = item.searchTerm;
                                filterBox.dispatchEvent(new Event('input'));

                                // Wait more than 250 ms (that's the timer setting for the combobox) to select the items.
                                setTimeout(function() {
                                    let rows = newTab.document.querySelectorAll("#fraItemsList #chunk-0 .combo-select-row");
                                    rows[i].dispatchEvent(new Event('click'));
                                }, salestracker.COMBO_DELAY);    // TODO: Replace this with a mutation observer or something.
                            });
                        }
                    });
                    tempDiv.appendChild(tempEl);
                    salesCol1.appendChild(tempDiv);

                    return false;
                });
            });
        });
    },
    salesTableRows: function(category) {
        // This category may not exist for both on sale and sold items, so store a pointer to them if available, and point to a new empty object if not.
        let current = this.salesData.CURRENT.totals[category] || {};
        let sold = this.salesData.SOLD.totals[category] || {};
        // Combine the items for sale and those already sold in this category into one array, sort them alphabetically, and deduplicate them via a Set.
        let items = new Set(Object.keys(current).concat(Object.keys(sold)).sort());
        let rowsHtml = "";

        // Create a table data row for each item in this category.
        items.forEach((item) => {
            rowsHtml += `
              <tr>
                <td>${item}</th>
                <td>${current[item] || 0}</td>
                <td>${sold[item] || 0}</td>
              </tr>`
        });

        // I don't want to re-set up the item Set again outside this function, but need the count outside this function, but can only return a string (instead of an object)
        // because of its use in the template literal. So, store it in the overall object and fetch it on the other side.
        // Add a couple of extra to represent the header row and the category line.
        this.countHack = items.size + 2;

        return rowsHtml;
    },
    hudHtml: `
<div class="row">
  <div class="col-4"></div>
  <div class="col-4"><button type="button" class="col-12 h-100 btn btn-info" data-toggle="modal" data-target="#salestrackerDialog">Open Trade HUD</button></div>
</div>
<div id="salestrackerDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="salestrackerModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="salestrackerModalLabel">Trade HUD</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-4"></div>
            <div class="col-4"><button id="salestrackerClearSold" type="button" class="col-12 h-100 btn btn-warning">Clear Inbox Data</button></div>
          </div>
          <div class="row">
            <div id="allSalesCol1" class="col-4"></div>
            <div id="salestrackerTableCol1" class="col-4"></div>
            <div id="salestrackerTableCol2" class="col-4"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>`
};

let dex_script_url = window.location.href;
Wolvden_SalesTracker.loadData();
if (dex_script_url.endsWith("manage")) {
    Wolvden_SalesTracker.trackSale(Wolvden_SalesTracker.SOURCE_CURRENT);
    Wolvden_SalesTracker.displayHud();
} else if (dex_script_url.includes("inbox/read/")) {
    Wolvden_SalesTracker.trackSale(Wolvden_SalesTracker.SOURCE_SOLD);
}

