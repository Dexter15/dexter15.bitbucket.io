// ==UserScript==
// @name         Wolvden - Wares Tracker
// @namespace    dex.wolvden
// @version      1.1
// @description  Tracks items in your hoard (including buried) and adds a flag on the Raccoon Wares page and the Lunar Shoppe page for the items you already one at least one of.
// @author       Dex
// @match      https://www.wolvden.com/store/raccoon-wares
// @match      https://www.wolvden.com/store/lunar-shoppe
// @match      https://www.wolvden.com/hoard
// @match      https://www.wolvden.com/hoard/buried
// @downloadURL  https://dexter15.bitbucket.io/Wolvden/tampermonkey/Wolvden - Wares Tracker.user.js
// @updateURL    https://dexter15.bitbucket.io/Wolvden/tampermonkey/Wolvden - Wares Tracker.user.js
// ==/UserScript==/*

var Wolvden_WaresTracker = {
    logging: false,
    waresData: {},
    SOURCE_HOARD: "HOARD",
    SOURCE_BURIED: "BURIED",
    logText: function(text) {
        if (Wolvden_WaresTracker.logging) {
            console.log("dex.wolvden.waresTracker    " + text);
        }
    },
    loadData: function() {
        let waresDataString = localStorage.getItem("dex_wolvden_waresData");
        this.logText("Stringified data from storage: " + waresDataString);

        if (waresDataString != null) {
            this.waresData = JSON.parse(waresDataString);
        }
    },
    saveData: function() {
        let waresDataString = JSON.stringify(this.waresData);
        localStorage.dex_wolvden_waresData = waresDataString;
    },
    trackItems: function(source) {
        // Get the textual content of the script that loads the hoard data.
        let scriptString = runOnDOMReady[2].toString();
        let rgxitems = /var perPageLimit = \d+;[^\n]*\n\s*(.*?)\n\s*var activeItems = \[];/i;

        let result = rgxitems.exec(scriptString);
        if (result != null) {
            // Found the items data variable.
            let itemStr = result[1];
            // Evaluate that so that the textual content becomes actual JavaScript data in this scope.
            eval(itemStr);

            let thisWaresData = [];
            // Loop through that JavaScript data.
            itemData.forEach((item) => {
                // Pull out the "html" property of this item's data, as it is the only thing that contains the item id.
                // (This property is set up as an array, but there is only ever one element present.)
                let dataHtml = item.html[0];
                // Evaluate that html into a temporary, in-memory jQuery object, so we can then find/get the checkbox input element.
                let jqObj = $(dataHtml).find("input[type='checkbox']");
                // Get the item id.
                let itmId = jqObj.attr("value");

                this.logText("Adding item: " + itmId + "; source: " + source);
                thisWaresData.push(itmId);
            });

            this.waresData[source] = thisWaresData;
            this.saveData();
        }
    },
    flagItems: function() {
        // Loop through the items to buy.
        $("div.item").each((index, obj) => {
            // Pull out the string that contains the item id.
            let itmStr = $(obj).find("select").attr("name");
            let rgxitemnum = /item\[([^\]]*)]/i;

            let result = rgxitemnum.exec(itmStr);
            if (result != null) {
                let itmId = result[1];
                if (this.isInWaresData(itmId)) {
                    // This item is in our hoard. Change the background to indicate that we already own it.
                    this.logText("Found owned item: " + itmId);
                    $(obj).css("background-color", "antiquewhite");
                }
            }
        });
    },
    isInWaresData: function(itmId) {
        for (const [key, value] of Object.entries(this.waresData)) {
            if (value.includes(itmId)) {
                return true;
            }
        }
    }
};

let dex_script_url = window.location.href;
Wolvden_WaresTracker.loadData();
if (dex_script_url.endsWith("hoard")) {
    Wolvden_WaresTracker.trackItems(Wolvden_WaresTracker.SOURCE_HOARD);
} else if (dex_script_url.endsWith("buried")) {
    Wolvden_WaresTracker.trackItems(Wolvden_WaresTracker.SOURCE_BURIED);
} else if (dex_script_url.endsWith("raccoon-wares") || dex_script_url.endsWith("lunar-shoppe")) {
    Wolvden_WaresTracker.flagItems();
}

