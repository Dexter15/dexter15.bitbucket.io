// ==UserScript==
// @name         Wolvden - Wares Hider
// @namespace    dex.wolvden
// @version      1.0
// @description  Takes the configured list of items and hides them from the sell boxes on the Raccoon Wares page.
// @author       Dex
// @include      https://www.wolvden.com/store/raccoon-wares
// @downloadURL  https://dexter15.bitbucket.io/Wolvden/tampermonkey/Wolvden - Wares Hider.user.js
// @updateURL    https://dexter15.bitbucket.io/Wolvden/tampermonkey/Wolvden - Wares Hider.user.js
// ==/UserScript==/*

var Wolvden_WaresFilter = {
    logging: true,
    logText: function (text) {
        if (Wolvden_WaresFilter.logging) {
            console.log("dex.wolvden.waresFilter    " + text);
        }
    },
    unwantedCategories: ["Healing", "Herb"],
    unwantedItems: ["Blue Jay Feather", "Bone", "Duck Feather", "Grouse Feather", "Gyrfalcon Feather", "Ibis Feather", "Large Branch", "Large Leaf", "Large Rock", "Mollusk Shell", "Owl Feather", "Owl Talon", "Peregrine Falcon Feather", "Puffin Feather",
                    "Roadrunner Feather", "Skua Feather", "Swan Feather", "Toucan Feather", "Ebony Jewelwing", "Arctic White", "Spicebush Swallowtail", "Tortoise Shell", "Turtle Shell", "Whale Rib Bone", "Gull Feather", "Rattle", "Parrot Feather",
                    "Baltimore Checkerspot", "Zebra Longwing", "Condor Feather", "Glowing Spores", "Gnawing Hoof", "Wolf Skull"],
    origComboSelect: unsafeWindow.comboSelect,
    filteredComboSelect: function(options) {
        //console.log(options);
        Wolvden_WaresFilter.logText("target id: " + options.target.id);
        if (options.target.id === "cboItemSelector" || options.target.id === "cboItemStackSelector") {
            //console.log(options.data);
            // Filter data for individual selector.
            options.data = options.data.filter(item => {
                if (Wolvden_WaresFilter.unwantedCategories.includes(item.category) || Wolvden_WaresFilter.unwantedItems.includes(item.name)) {
                    return false;
                }
                return true;
            });
        }
        Wolvden_WaresFilter.origComboSelect(options);
    }
}

unsafeWindow.comboSelect = Wolvden_WaresFilter.filteredComboSelect;

// Clone the existing combobox in order to remove the JavaScript event handlers tied to it (so they don't fire twice, once for the original and once for the recreated combobox).
// Thanks to https://javascriptio.com/view/3110929/remove-already-existing-event-listener for this trick.
let element = $("#cboItemSelector")[0];
let newElement = element.cloneNode(true); // true = deep
element.parentNode.insertBefore(newElement, element);
element.parentNode.removeChild(element);

element = $("#cboItemStackSelector")[0];
newElement = element.cloneNode(true); // true = deep
element.parentNode.insertBefore(newElement, element);
element.parentNode.removeChild(element);

// Rerun the function that sets up the data for the list box and calls the function (now swapped to our version) to create the list box.
runOnDOMReady[2]();
